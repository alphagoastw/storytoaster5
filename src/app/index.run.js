(function() {
  'use strict';

  angular
    .module('storyToaster')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log,
                    $http,
                    $rootScope,
                    deviceDetector,
                    config,
                    authService,
                    editableOptions,
                    editableThemes) {

    $http.defaults.headers.common[config.apiKeyName] = config.apiKeyValue;
    $http.defaults.headers.common[config.authTokenName] = "Bear " +  authService.getAuthToken();

    //theme options for xeditable - edit computer name plugin
    editableOptions.theme = 'bs3';
    editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span></button>';

    $log.debug('runBlock end');

    $rootScope.deviceDetector = deviceDetector;
    $log.debug('device=', deviceDetector.raw);

  }
})();
