/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController($log,
                           authService,
                           $modalStack,
                           $modal,
                           $modalInstance,
                           commonTools,
                           onLoginSucceed,
                           modalOptions) {
    $log = $log.withTag('Login Ctrl');

    var vm = this;

    vm.message = 'from login.controller.js';

    vm.customerLogin = customerLogin;
    vm.openForgotPassword = openForgotPassword;
    vm.closeModal = closeModal;
    vm.modalOptions = modalOptions || {};
    vm.auth = {};

    vm.auth.$error = {
      "authFailed": false
    };

    activate();

    ////////////////

    function activate() {
    }//end activate()

    function customerLogin(formvalid) {
      var handler = onLoginSucceed;
      $log.debug('customerLogin called');
      if (!vm.customer || !vm.customer.email || !vm.customer.password) {
        $log.error("Data error.");
        return -1;
      }

      authService.customerLogin(vm.customer.email, vm.customer.password)
        .then(function (succeed) {
          vm.auth = null;
          if (succeed) {
            // succeed
            $log.debug("login.controller:", succeed);
            $modalStack.dismissAll();
            handler();
          }
          else {
            $modalStack.dismissAll();
            //Open reset password dialog
            $log.debug("login.controller Reset Temporary Password");
            var modalInstance = $modal.open({
              templateUrl: 'app/content/login/reset-password/reset-password.html',
              controller: 'ResetPasswordController',
              controllerAs: 'resetPassword',
              resolve: {
                customerEmail: function () {
                  return vm.customer.email;
                },
                onLoginSucceed: function () {
                  return handler;
                }
              }
            });//end
          }
        },
        function (err) {
          $log.debug("Login failed show validation message");
          vm.auth = commonTools.toApiError(err, 401);
        }
      )
        .catch(handleError);
    }//end customerLogin()

    /**
     * Open modal dialog
     */
    function openForgotPassword() {

      $modalStack.dismissAll();
      $log.debug('login.openForgotPassword()');

      //open dialog for ForgotPassword
      var modalInstance = $modal.open({
        templateUrl: 'app/content/login/forgot-password/forgot-password.html',
        controller: 'ForgotPasswordController',
        controllerAs: 'forgotPassword'
      });

    }//end openForgotPassword()

    function handleError(err) {
      $log.error("customerLogin fail:", err);
    }


    /**
     *
     */
    function closeModal() {
      $modalInstance.close();
    }//end closeModal()
  }//end LoginController();

})();

