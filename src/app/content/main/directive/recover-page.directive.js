(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('recoverPage', recoverPage);

  /* @ngInject */
  function recoverPage($window,$rootScope, deviceDetector,$log,$timeout) {

    var directive = {
      templateUrl: 'app/content/main/template/recover-book-page.html',
      link: link,
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {
      $timeout(function () {
        var t = ($('.editor')).width();
        var w = (t - 40) / 2;
        var h = w / 1.375;

        var canvasWidth = w - 10;
        var canvasHeight = h - 10;

        $(".page-big").css('height', h + "px");
        $(".page-big").css('width', w + "px");
        var canvaes = $(".recover_canvas");
        if (canvaes != null) {
          var css = canvaes.map(function (c) {
            var c = new fabric.Canvas(c.id, {select: false, backgroundColor: '#ffffff'});
                c.setWidth(canvasWidth);
                c.setHeight(canvasHeight);
            return c;
          });
          scope.main.setRecoverCanvasArray(css);
        }
      });
    }
  }

})();


