/**
 * Created by Administrator on 2016/10/9.
 */

(function(global) {

  "use strict";

  fabric.StoryTextbox = fabric.util.createClass(fabric.Textbox, {

    type: 'storyTextbox',
    src: '',

    flipH: false,
    flipV: false,

    initialize: function (text, options) {
      text = " \n\n\n\n\n\n                                      " +
        "                                                                      \n\n\n \n";

      //text = "      " +
      //  " ";
      options || (options = {left: 40, top: 40});

      //this.width = options.width > 568 ? options.width : 568;
      //this.height = options.height > 356 ? options.height : 356;

      var thisTextObject = this;
      Object.getOwnPropertyNames(options).forEach(
        function (prop) {
          thisTextObject[prop] = options[prop];
          console.log(options[prop]);
        }
      );

      this.left = options.left >40 ? options.left : 40;
      this.top = options.top > 40 ? options.top : 48;

      this.callSuper('initialize', text);
      //this.fontSize = options.fontSize;

      this.image = new Image();
      this.image.crossOrigin = "Anonymous";
      this.image.src = options.src;

      this.image.onload = (function () {
        this.loaded = true;
        this.setCoords();
         this.image.width =  this.width ;
         this.image.height = this.height;

        this.fire('image:loaded');
        if (this.canvas)
          this.canvas.renderAll();
      }).bind(this);
    },

    toObject: function () {
      return fabric.util.object.extend(this.callSuper('toObject'), {
        label: this.get('label'),
        src: this.image.src
      });
    },

    _render: function (ctx) {
      console.log('rendering storyTextbox', this.left);
      if (this.loaded) {
        //   ctx.drawImage(this.image, -this.width / 2 - 20, -this.height / 2 - 10, this.width + 40, this.height + 40);

        var scaleX =  1, // Set horizontal scale to -1 if flip horizontal
          scaleY = 1, // Set verical scale to -1 if flip vertical
          posX = 0, // Set x position to -100% if flip horizontal
          posY = 0; // Set y position to -100% if flip vertical

        ctx.save(); // Save the current state
        ctx.scale(scaleX, scaleY); // Set scale to flip the image

        ctx.drawImage(this.image, posX - this.width / 2, posY - this.height / 2, this.width, this.height); // draw the image
        ctx.restore(); // Restore the last saved state

      }

      this.callSuper('_render', ctx);
    }
  });

  fabric.StoryTextbox.fromObject = function (object) {
    var instance = new fabric.ImageTextbox(object.text, fabric.util.object.clone(object), function () {
      return instance && instance.canvas && instance.canvas.renderAll();
    });
    return instance;
  };

})( typeof exports != 'undefined' ? exports : this);

