/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('login', login);


  /* @ngInject */
  function login() {
    var directive = {
      bindToController: true,
      controller: 'LoginController',
      controllerAs: 'login',
      templateUrl: 'app/content/login/login.html',
      restrict: 'A',
      scope: {}
    };
    return directive;

  }//end login()

})();

