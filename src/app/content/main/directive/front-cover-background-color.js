(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('bkColorPicker', bkColorPicker);

  /* @ngInject */
  function bkColorPicker() {
    var directive = {
      link: link,
      restrict: 'AE',
      templateUrl:'/app/content/main/template/front-cover-background-color.html'
    };
    return directive;

    function link(scope, element, attrs) {

      var items = angular.element(element[0].querySelectorAll('.ql-picker-item'));
      items.bind("click", function (evt) {

        if (evt.target.attributes['data-value']) {
          var color = evt.target.attributes['data-value'].value;

          if (color !== "") {
            scope.main.PhotoBook.backgroundColor = color;
            scope.$apply();
          }
        }
      })

    }
  }


})();

