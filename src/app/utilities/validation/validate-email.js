/**
 * Created by bwong on 10/28/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('validateEmail', validateEmail);

  /* @ngInject */
  function validateEmail(authService) {
    var directive = {
      link: link,
      require: 'ngModel',
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs, ctrl) {
      //var emailPattern = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/; //this will allow .co.uk, but not extended domains - .commmmmmm

      //Add async validator to Angular. Return a promise, Angular will set $validitiy on ngModel.
      //promise.resolve = valid, promise.reject = not valid
      ctrl.$asyncValidators.emailExists = function doesEmailExist(modelValue){
        if(attrs.uniqueEmail == 'true' && !ctrl.$error.pattern && !ctrl.$error.required){
          return authService.checkEmailExists(modelValue);
        }
      };

    }//end link()

  }//end validateEmailPattern()

})();

