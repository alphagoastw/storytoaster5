(function() {
  'use strict';

  angular
    .module('storyToaster')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app.example', {
        url: '/example',
        controller: 'ExampleController',
        controllerAs: 'example',
        templateUrl: 'app/content/examples/example-layout.html'
      })
      .state('app', {
        abstract: true,
        templateUrl: 'app/layouts/app/app.html',
        resolve: {
        }
      })
      .state('app.home', {
        url: '/',
        templateUrl: 'app/content/home/home.html',
        controller: 'HomePageController',
        controllerAs: 'homePage'
      })

      .state('app.editBook',{
        url:'/edit/:bookId',
        templateUrl:'app/content/main/main.html',
        controller:'MainController',
        controllerAs:'main'
      })
      .state('app.mybooks',{
        url:'/mybooks',
        templateUrl:'app/content/books/my-books-list.html',
        controller:'BookController',
        controllerAs:'bookCtrl'
      })
      .state('app.readbook',{
        url:'/mybooks/:id/read',
        templateUrl:'app/content/books/templates/sj.html',
        controller:'ReadbookController',
        controllerAs:'readbookCtrl',
        reloadOnSearch:false,
        resolve:{
          bookSelected :function($stateParams,bookRepository){
            var bookId = $stateParams.id;
            return bookRepository.getBookById(bookId);
          }
        }

      })

      .state('app.account', {
        url: '/account',
        template: '<div ui-view></div>',
        abstract: true
      })
      .state('app.registration', {
        url: '/registration',
        abstract: true,
        templateUrl: 'app/content/registration/registration.html',
        controller: 'RegistrationController',
        controllerAs: 'registration'
      })
      .state('app.registration.createaccount', {
        url: '/createacount?regCode&locale&fromSource',
        templateUrl: 'app/content/registration/create-account/registration-create-account.html',
        controller: 'RegistrationCreateAccountController',
        controllerAs: 'regCreateAccount',
        bindToController: true
      })
      .state('app.registration.settings', {
        url: '/settings',
        templateUrl: 'app/content/registration/setting/settings.html',
        controller: 'SettingsController',
        controllerAs: 'settings'
      })

      .state('app.account.setup', {
        url: '/setup',
        templateUrl: 'app/content/account/settings/account-settings.html',
        controller: 'AccountSettingsController',
        controllerAs: 'accountSettings'
      })
      .state('app.account.settings', {
        url: '/settings',
        templateUrl: 'app/content/account/settings/settings.html',
        abstract: true,
        controller:'AccountSettingsController',
        controllerAs: 'accountSetting'
      })
      .state("app.test",{
        url:'/test',
        templateUrl:'app/content/fabric/test.html',
        controller:'fabricTestController'
      })
      ;

    //$urlRouterProvider.when('/account/settings', '/account/settings/login-info');
    $urlRouterProvider.otherwise('/');
  }

  /** @ngInject */
  function authenticatedUserOnlyResolver() {
    return {
      /** @ngInject */
      authenticated: function (authService, $state, $q, $timeout) {
        if (authService.isAuthenticated()) {
          return $q.when();
        } else {
          $timeout(function () {
            $state.go("app.home");
          });
          return $q.reject();
        }
      }
    }
  }



})();
