(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('authToken', authToken);


  /* @ngInject */
  function authToken($sessionStorage, config, $http) {

    var authenticationToken = {
      setToken: setToken,
      getToken: getToken
    };

    return authenticationToken;

    ///////////////
    function setToken(token) {
      $http.defaults.headers.common[config.authTokenName] = token;
      $sessionStorage.token = token;
    }

    function getToken() {
      return $sessionStorage.token;
    }
  }

})();
