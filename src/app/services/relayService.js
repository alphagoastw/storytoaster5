/**
 * Created by Brett Wang on 30/05/2015.
 */

(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('relayService', relayService);

  relayService.$inject = ['$window'];

  /* @ngInject */
  function relayService($window) {
    var storage = $window.localStorage;
    var bag = {};

    var service = {

      get: get,
      put: put,
      putKeyValue: putKeyValue,
      getKeyValue: getKeyValue,
      clearKeyValue: clearKeyValue,
      clear: clear
    };
    return service;

    ////////////////

    function put(obj) {
      bag = obj;
    }

    function putKeyValue(key, obj) {
      storage.setItem(key, JSON.stringify(obj));
    }

    function get() {
      return bag;
    }

    function clearKeyValue(key) {
      storage.removeItem(key);
    }

    function getKeyValue(key) {
      var obj = storage.getItem(key);
      if (obj) {
        return JSON.parse(storage.getItem(key));
      } else {
        return "";
      }
    }

    function clear() {
      bag = null;
      storage.clear();
    }
  }

})();
