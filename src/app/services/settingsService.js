/**
 * Created by byan on 11/2/2015.
 */
(function(){

  'use strict';

  angular.module('storyToaster')
    .factory('settingsService', SettingsService);

  function SettingsService($http, $q, config, loca) {

    function RemoteService() {
    }

    RemoteService.prototype.getEusaText = getEusaText;
    RemoteService.prototype.isEulaAccepted = isEulaAccepted;
    RemoteService.prototype.updateEulaAcceptionDate = updateEulaAcceptionDate;
    RemoteService.prototype.getSetupConfiguration = getSetupConfiguration;
    RemoteService.prototype.saveSetupConfiguration = saveSetupConfiguration;
    RemoteService.prototype.getCustomerProfile = getCustomerProfile;
    RemoteService.prototype.updateCustomerInfo = updateCustomerInfo;
    RemoteService.prototype.updateCustomerPin = updateCustomerPin;
    RemoteService.prototype.getSettingLanguages = getSettingLanguages;

    function getSettingLanguages() {
      var defer = $q.defer();
      var url = config.apiRootPath + 'account/setting/language';

      $http.get(url).then(
        function(res){
          return defer.resolve(res.data);
        },
        function(err){
          return defer.reject([]);
        }
      );

      return defer.promise;
    }

    function getCustomerProfile(customerId) {

      var url = config.apiRootPath + 'customer/' + customerId + "/profile?locale=" + loca.getLocale();

      return $http.get(url).then(
        function (res) {
          return res.data;
        },
        function(err){
          return null;
        }
      );
    }

    /**
     *
     * @param customerId
     * @param data
     * @returns {*}
     */
    function updateCustomerInfo(customerId, data) {
      var url = config.apiRootPath + 'customer/' + customerId + '/profile';

      return $http.put(url, data);
    }//end updateCustomerInfo()


    /**
     *
     * @param customerId
     * @param pin
     * @returns {*}
     */
    function updateCustomerPin(customerId, pin){
      var url = config.apiRootPath + 'customer/' + customerId + '/pin';

      return $http.put(url, pin);
    }//end updateCustomerPin()



    function getEusaText (locale) {
      var url = config.apiRootPath + 'eulas/' + locale;

      return $http.get(url).then(
        function (res) {
          return res.data;
        }
      );
    }

    function isEulaAccepted (customerId) {
      var url = config.apiRootPath + 'customers/' + customerId + '/eula';

      return $http.get(url).then(
        function (res) {
          return res.data.isEulaAccepted;
        }
      );
    }

    function updateEulaAcceptionDate (customerId) {
      var url = config.apiRootPath + 'customers/' + customerId + '/eula';

      return $http.post(url).then(
        function (res) {
          return res.data;
        }
      );
    }

    function getSetupConfiguration (customerId) {
      var url = config.apiRootPath + 'customers/' + customerId + '/setupconfig';

      return $http.get(url).then(
        function (res) {
          return res.data;
        }
      );
    }

    function saveSetupConfiguration (customerId, setupConfig) {
      var url = config.apiRootPath + 'customers/' + customerId + '/setupconfig';

      if (setupConfig.requirePinSet) {
        setupConfig.dateOfBirth = moment(setupConfig.dateOfBirth).toISOString();
      }

      return $http.post(url, setupConfig).then(
        function (res) {
          return res.data;
        }
      );
    }

    return new RemoteService();

  }//end SettingsService()

})();
