(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('doubleDesignPage', doubleDesignPage);

  /* @ngInject */
  function doubleDesignPage($window,$rootScope, deviceDetector,$log) {

    var directive = {
      templateUrl:  'app/content/main/template/double-design-page.html',
      link: link,
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {
      $log.log('doubleDesignPage created');
      $log.log("device type=", deviceDetector.device);

      scope.onResize = function () {

        //var t = ($('#bigPagePanel')).width();
        var t = ($('.editor')).width();
        var w = (t - 40) / 2;
        var h = w / 1.375;

        var canvasWidth = w -10;
        var canvasHeight = h -10;

        $(".page-big").css('height', h + "px");
        $(".page-big").css('width', w + "px");

        if(!scope.main.left_canvas || !scope.main.right_canvas) {
          scope.main.left_canvas = new fabric.Canvas('left_canvas', {select: false, backgroundColor: '#ffffff'});
          scope.main.right_canvas = new fabric.Canvas('right_canvas', {select: false, backgroundColor: '#ffffff'});
          scope.main.currentCanvas = scope.main.left_canvas;
        }

        scope.main.left_canvas.setWidth(canvasWidth);
        scope.main.left_canvas.setHeight(canvasHeight);

        scope.main.right_canvas.setWidth(canvasWidth);
        scope.main.right_canvas.setHeight(canvasHeight);

        $rootScope.$broadcast('designCanvasInitialed')
      };
      scope.onResize();

      angular.element($window).bind('resize', function () {
        console.log('in resize event')
        scope.onResize();
      })
    }
  }

})();

