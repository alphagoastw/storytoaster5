(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('deviceRepository', deviceRepository);

  /* @ngInject */
  function deviceRepository($http,
                            $q,
                            config,
                            authService,
                            licenseService) {

    var service = {
      getCustomerDevices: getCustomerDevices,
      getDeviceByEsn: getDeviceByEsn,
      updateLicense: updateLicense
    };

    return service;

    ////////////////

    function updateLicense(esn, data) {
      var customerId = authService.getUserId();
      var url = config.apiRootPath + 'customer/' + customerId + '/device/' + esn;
      return $http.put(url, data)
        .then(function (res) {
          var license = res.data;
          if (license) {
            licenseService.updateLicenseData(license);
          }
          return license;
        });
    }

    function getDeviceByEsn(esn) {
      var customerId = authService.getUserId();
      var url = config.apiRootPath + 'customer/' + customerId + '/device/' + esn;
      return $http.get(url)
        .then(function (res) {
          var license = res.data;
          if (license) {
            licenseService.updateLicenseData(license);
          }
          return license;
        });
    }

    function getCustomerDevices() {
      var customerId =  authService.getUserId();
      var url = config.apiRootPath + 'customer/' + customerId + '/device';

      return $http.get(url)
        .then(function (res) {
          var licenseDetails = res.data;
          if (licenseDetails && angular.isArray(licenseDetails.licenses)) {
            licenseService.updateLicenseData(licenseDetails.licenses);
          } else {
          }
          return licenseDetails;
        });
    }//end getCustomerDevices()

  }//end devicesRepository()

})();


