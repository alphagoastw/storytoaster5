/**
 * Created by bwong on 1/22/16.
 */
(function() {
  'use strict';

  angular
    .module('storyToaster')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $templateCache) {

    $templateCache.put('template/tabs/tabset.html',
      "<div class=\"clearfix\"><ul class=\"tab-nav\" ng-class=\"{'tn-vertical': vertical, 'tn-justified': justified, 'tab-nav-right': right}\" ng-transclude></ul><div class=\"tab-content\"><div class=\"tab-pane\" ng-repeat=\"tab in tabs\" ng-class=\"{active: tab.active}\" tab-content-transclude=\"tab\"></div></div></div>"
    );

    $log.debug('tabset runBlock end');
  }
})();
