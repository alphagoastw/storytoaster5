/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular.module('storyToaster')
    .controller('FooterController', FooterController);


  /* @ngInject */
  function FooterController() {
    var vm = this;
    vm.title = '';

    activate();

    ////////////////

    function activate() {
      vm.title = 'FooterController';
    }
  }

})();



