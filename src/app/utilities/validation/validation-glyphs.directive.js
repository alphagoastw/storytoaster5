/**
 * Created by bwong on 10/23/15.
 */
(function () {
  'use strict';

  angular.module('storyToaster')
    .directive('validationGlyphs', validationGlyphs);


  /* @ngInject */
  function validationGlyphs() {
    var directive = {
      link: link,
      restrict: 'A',
      scope: {
        field: '=observe'
      }
    };
    return directive;

    function link(scope, element) {

      //get the span and input elements
      var span = element[0].querySelector('span.zmdi');
      var input = element[0].querySelector('.form-control');

      //create new angular jQLite object to use
      var glyphElement = angular.element(span);
      var inputElement = angular.element(input);

      var focused = false;


      //if focus is on, and it is invalid, add warning (error classes when focus is off)
      inputElement.bind('focus', function(){
        scope.$apply(function(){
          focused = true;
          updateValidity();
        });
      });//end element.bind('focus')


      //if focus is left, and it is invalid, add error class (warning class is when you are focused)
      inputElement.bind('blur', function(){
        scope.$apply(function(){
          focused = false;
          updateValidity();
        });
      });//end element.bind('blur')

      scope.$watch('field.$$parentForm.$submitted', updateValidity);
      scope.$watch('field.$dirty', updateValidity);

      scope.$watch('field.$valid', updateValidity);//end $watch()

      function updateValidity(newValue) {
        var valid = scope.field.$valid;
        if (angular.isUndefined(valid) || valid === null) {
          // This happens to async validators
          valid = true;
        }
        //if this has changed, and it is valid
        if (scope.field.$dirty || scope.field.$$parentForm.$submitted) {
          if (valid) {
            //add has-success, remove warning
            element.addClass('has-success').removeClass('has-warning').removeClass('has-error');

            //add CHECK MARK icon, remove X icon
            glyphElement.addClass('zmdi-check').removeClass('zmdi-close');
          } else {
            //add warning when filling out form if it is invalid
            element.removeClass('has-success');
            if (focused) {
              element.addClass('has-warning');
            } else {
              element.addClass('has-error');
            }
            //add X icon, remove CHECK MARK icon
            glyphElement.addClass('zmdi-close').removeClass('zmdi-check');
          }
        } else {
          element.removeClass('has-warning').removeClass('has-success').removeClass('has-error');
          glyphElement.removeClass('zmdi-close').removeClass('zmdi-check');
        }


      }
    }//end link()
  }


})();
