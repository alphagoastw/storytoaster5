(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('authService', authService);

  /* @ngInject */
  function authService($http,
                       $cookieStore,
                       $q,
                       authToken,
                       config,
                       $sessionStorage,
                       $log,
                       $rootScope,
                       $state,
                       broadcastEvents)
  {

    var cookieKey = "storyToaster";
    var API_URL = config.apiRootPath;
    var clientKey = config.apiKeyValue;

    var service = {
      getUser: getUser,
      getAuthToken: getAuthToken,
      getUserId:getUserId,
      isAuthenticated:isAuthenticated,
      login:login,
      register:register,
      checkEmailExists:checkEmailExists,
      customerLogin:customerLogin,
      logOut:logOut,
      passwordReminder: passwordReminder,
      consumerLogin : consumerLogin,
      saveNewPassword: saveNewPassword,
      currentUser:currentUser,
      sessionToken:sessionToken

      };
    return service;

    ////////////////

    function getAuthToken() {
      return authToken.getToken();
    }

    function getUserId() {
      var user = $sessionStorage.user;
      return user ? user.customerId : -1;
    }

    function getUser() {
      return $sessionStorage.user;
    }

    function isAuthenticated() {
      //TODO add API call to validate token

      var token = authToken.getToken();
      if (token) {
        return true;
      } else {
        return false;
      }
    }

    function consumerLogin(email, password) {
      var API_URL = config.apiRootPath + 'auth/customer';

      return $http.post(API_URL, data).then(
        function success(res) {
          if (res.status == 200) {
            //authToken.setToken(res.data.authToken);
            //relayService.putKeyValue('userName',domainUser);
            return res;
          }
        });
    }


    /**
     * returns a promise, resolved if email does not exist, rejected if email exists.
     * @param email
     * @returns {jQuery.promise|promise.promise|d.promise|promise|.ready.promise|jQuery.ready.promise|*}
     */
    function checkEmailExists(email){
      var dfd = $q.defer();
      var url = API_URL + "auth/checkemail/" + email;

      $http.get(url).then(
        function (res) {
          //$log.debug("authService.checkEmailExists res:", res);
          if (res.status == 200 && res.data === true) {
            dfd.reject(false);
          }
          else {
            dfd.resolve(true);
          }
        },
        function (err) {
          dfd.reject(false);
        }
      );

      return dfd.promise;
    }

    function login (domainUser, password) {
      var dfd = $q.defer();

      var url = API_URL + "auth/windowsAuth";
      var data = {user: domainUser + '@absolute.com', password: password};

      $http.post(url, data).then(
        function (res) {
          if (res.status == 200) {
            authToken.setToken(res.data.authToken);
            $log.debug(res.data);
            $sessionStorage.user = res.data;
            $rootScope.$broadcast(broadcastEvents.GLOBAL_LOGIN_CHANGED, res.data);
            dfd.resolve(res);
          }
          else {
            dfd.reject(res);
          }
        },

        function (err) {
          dfd.reject(err);
        }
      );

      return dfd.promise;
    }

    /**
     * returns a promise, resolved if login, rejected if login is not successful
     * @param email, password
     */
    function customerLogin(email, password) {
      var url = API_URL + "auth/login";
      var data = {email: email, password: password};
      var dfd = $q.defer();
      $http.post(url, data)
        .then(function (res) {
          //check for Temporary password
            authToken.setToken(res.data.sessionToken);
            $sessionStorage.user = res.data;
            $rootScope.$broadcast(broadcastEvents.GLOBAL_LOGIN_CHANGED, res.data);
            authSuccessful(res.data);
            dfd.resolve(true);
        },
        function (err) {
          dfd.reject(err);
        });
      return dfd.promise;
    }

    /**
     * Send password reminder
     * @param email
     */
    function passwordReminder (email) {
      var dfd = $q.defer();

      var url = API_URL + "auth/passwordReminder";
      var data = {email: email};

      $http.post(url, data).then(
        function (res) {
          if (res.status == 200) {
            authToken.setToken(res.data.sessionToken);
            $sessionStorage.user = res.data;
            $rootScope.$broadcast(broadcastEvents.GLOBAL_LOGIN_CHANGED, res.data);
            dfd.resolve(true);
          }
          else {
            dfd.reject(res);
          }
        },
        function (err) {
          dfd.reject(err);
        }
      );

      return dfd.promise;
    }

    /**
     * Save new password
     * @param email
     */
    function saveNewPassword (email, newpassword) {
      var dfd = $q.defer();
      $log.debug('saveNewPassword authserv called', email);
      var url = API_URL + "auth/saveNewPassword";
      var data = {email: email, newpassword: newpassword};

      $http.post(url, data).then(
        function (res) {
          if (res.status == 200) {
            dfd.resolve(true);
          }
          else {
            dfd.reject(res);
          }
        },
        function (err) {
          dfd.reject(err);
        }
      );

      return dfd.promise;
    }

    function logOut(){
      $sessionStorage.$reset();
      $rootScope.$broadcast(broadcastEvents.GLOBAL_LOGIN_CHANGED, null);
    }

    function register(userInfo){
      //TODO set the locale for the user from system variable
      userInfo.locale = 'en-US';
      var dfd = $q.defer();
      var url = API_URL + "customer";
      $http.post(url, userInfo).then(

        function (res) {
          if (res.status == 201) {
            authToken.setToken(res.data.authToken);
            var user = {
              customerId:res.data.customerId,
              firstName:userInfo.firstName,
              lastName: userInfo.lastName
            };
            $sessionStorage.user = user;
            $rootScope.$broadcast(broadcastEvents.GLOBAL_LOGIN_CHANGED, res.data);
            dfd.resolve(res.data);
          }
          else {
            dfd.reject(res.data);
          }
        },

        function (err) {
          dfd.reject(err);
        }
      );

      return dfd.promise;
    }

    function currentUser () {
      return $cookieStore.get(cookieKey);
      return $sessionStorage.user;
    };

     isAuthenticated = function () {
      return !!authToken.getToken();
    };

    function sessionToken() {
      return authToken.getToken();
    }

    function authSuccessful(data) {
      authToken.setToken(data.token);
      $cookieStore.put(cookieKey, data.user);
    }
  }

})();



