/**
 * Created by bwong on 4/14/16.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('hideMenuItemsService', hideMenuItemsService);


  /* @ngInject */
  function hideMenuItemsService($log) {
    $log = $log.withTag('hideMenuItemsService');

    this.deviceList = true;
    this.settings = true;

    var service = {
      hideDeviceList: hideDeviceList,
      getDeviceListState: getDeviceListState,
      hideSettings: hideSettings,
      getSettingsState: getSettingsState
    };

    return service;

    ////////////////

    function hideDeviceList(boolean){
      this.deviceList = boolean;
    }//end setDeviceListState()

    function getDeviceListState() {
      return !this.deviceList;
    }//end hideDeviceListLink()

    function hideSettings(boolean){
      this.settings = boolean;
    }//end setSettingsState()

    function getSettingsState(){
      return !this.settings;
    }//end hideSettingsLink()
  }

})();

