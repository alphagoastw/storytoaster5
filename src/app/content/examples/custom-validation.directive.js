/**
 * Created by bwong on 10/19/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('customValidation', customValidation);


  /* @ngInject */
  function customValidation() {
    var directive = {
      restrict: 'A',
      require: 'ngModel',
      link: link
    };
    return directive;

    function link(scope, element, attrs, ctrl) {
      scope.$watch(attrs.ngModel, function customValidationWatch(value){
        if(typeof value != "undefined"){

          if(value.length % 2 == 1){
            ctrl.$setValidity('customError', false);
          } else {
            ctrl.$setValidity('customError', true);
          }

        }


      });//end $watch()

    }//end link()
  }//end customValidation


})();

