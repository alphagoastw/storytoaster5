/**
 * Created by bwong on 10/29/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('dateRange', dateRange);

  /* @ngInject */
  function dateRange(moment,
                     $log) {
    $log = $log.withTag('dateRange');
    var directive = {
      link: link,
      require: 'ngModel',
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs, ctrl) {
      ctrl.$validators.dateRange = function doesFieldMatch(modelValue) {
        var maxDate = moment(scope.$eval(attrs.drMax));
        var minDate = moment(scope.$eval(attrs.drMin));
        var currentDate = moment(modelValue);
        if (!maxDate.isValid()) {
          $log.error('drMax is invalid');
        } else if (!minDate.isValid()) {
          $log.error('drMin is invalid');
        } else if (!currentDate.isValid()) {
          return false;
        } else {
          minDate = minDate.add(-1, 'd');
          maxDate = maxDate.add(1, 'd');
          return currentDate.isBetween(minDate, maxDate);
        }
      };

    }//end link()

  }//end matchField()

})();

