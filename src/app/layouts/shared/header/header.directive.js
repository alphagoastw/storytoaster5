/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('header', header);

  /* @ngInject */
  function header() {
    var directive = {
      bindToController: true,
      templateUrl: 'app/layouts/shared/header/header.html',
      controller: 'HeaderController',
      controllerAs: 'header',
      restrict: 'A'
    };
    return directive;
  }//end header()

})();//end function()

