'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');

gulp.task('inject', ['scripts', 'styles', 'copyFonts','copyVendor'], function () {
  var injectStyles = gulp.src([
    path.join(conf.paths.tmp, '/serve/app/**/*.css'),
    path.join('!' + conf.paths.tmp, '/serve/app/vendor.css'),
    path.join('!' + conf.paths.tmp, '/vendor/turnjs/**/*.css')
  ], { read: false });

  var injectScripts = gulp.src([
    path.join(conf.paths.src, '/app/**/*.module.js'),
    path.join(conf.paths.src, '/app/**/*.js'),
    path.join('!' + conf.paths.src, '/app/**/*.spec.js'),
    path.join('!' + conf.paths.src, '/app/**/*.mock.js'),
    path.join('!' + conf.paths.src, '/vendor/turnjs/**/*.js')

  ])
  .pipe($.angularFilesort()).on('error', conf.errorHandler('AngularFilesort'));

  var injectOptions = {
    ignorePath: [conf.paths.src, path.join(conf.paths.tmp, '/serve')],
    addRootSlash: false
  };

  return gulp.src(path.join(conf.paths.src, '/*.html'))
    .pipe($.inject(injectStyles, injectOptions))
    .pipe($.inject(injectScripts, injectOptions))
    .pipe(wiredep(_.extend({}, conf.wiredep)))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve')));


});

gulp.task('copyFonts', function () {
  gulp.src([
    path.join(conf.paths.src, '/../bower_components/fontawesome/fonts/*.*'),
    path.join(conf.paths.src, '/../bower_components/material-design-iconic-font/dist/fonts/*.*'),
    path.join(conf.paths.src, 'assets/theme/bootstrap/fonts/*.*'),
    path.join(conf.paths.src, 'assets/fonts/*.*')
  ])
    .pipe(gulp.dest(conf.paths.tmp + '/serve/fonts'));
});

gulp.task('deployFonts', function(){
  gulp.src([
    path.join(conf.paths.src, '/../bower_components/fontawesome/fonts/*.*'),
    path.join(conf.paths.src, '/../bower_components/material-design-iconic-font/dist/fonts/*.*'),
    path.join(conf.paths.src, 'assets/theme/bootstrap/fonts/*.*'),
    path.join(conf.paths.src, 'assets/fonts/*.*')
  ])
    .pipe(gulp.dest(conf.paths.dist + '/fonts'));
});

gulp.task('copyVendor',function(){
  gulp.src([
    path.join(conf.paths.src,'vendor/**/*.*')
  ])
    .pipe(gulp.dest(conf.paths.dist+'/vendor/'));
});
