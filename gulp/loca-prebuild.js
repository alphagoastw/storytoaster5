/**
 * Created by JZhang on 10/02/2016.
 */

var es = require('event-stream');
var path = require('path');
var concat = require('gulp-concat');
var header = require('gulp-header');
var footer = require('gulp-footer');
var gutil = require('gulp-util');

var TEMPLATE_HEADER = 'angular.module("<%= module %>").service("<%= service %>", [function() { this.keyMap = {};';
var TEMPLATE_ITEM = 'this.keyMap["<%= url %>"]=<%= keys %>;';
var TEMPLATE_FOOTER = '}]);';
var DEFAULT_FILENAME = 'locaKeys.js';
var DEFAULT_MODULE = 'defaultModule';
var DEFAULT_SERVICE = 'locaKeyMap';

function locaPrebuildGulp(filename, options) {
  /**
   * Prepare options
   */

  if (typeof filename === 'string') {
    options = options || {};
  } else {
    options = filename || {};
    filename = options.filename || DEFAULT_FILENAME;
  }

  /**
   * Prepare header / footer
   */

  var templateHeader = options.templateHeader || TEMPLATE_HEADER;
  var templateFooter = options.templateFooter || TEMPLATE_FOOTER;

  return es.pipeline(
    es.map(locaPrebuildStream(options.root || '', options.base)),
    concat(filename),
    header(templateHeader, {
      module: options.module || DEFAULT_MODULE,
      service: options.serviceName || DEFAULT_SERVICE
    }),
    footer(templateFooter, {
      module: options.module || DEFAULT_MODULE
    })
  );
}

module.exports = locaPrebuildGulp;

/**
 * Add files to templateCache.
 */

function locaPrebuildStream(root, base) {

  return function locaKeyFile(file, callback) {
    if (file.processedByLocaPrebuild) {
      return callback(null, file);
    }

    var template = TEMPLATE_ITEM;
    var url;

    file.path = path.normalize(file.path);

    /**
     * Rewrite url
     */

    if (typeof base === 'function') {
      url = path.join(root, base(file));
    } else {
      url = path.join(root, file.path.replace(base || file.base, ''));
    }

    /**
     * Normalize url (win only)
     */

    if (process.platform === 'win32') {
      url = url.replace(/\\/g, '/');
    }

    /**
     * Create buffer
     */

    file.contents = new Buffer(gutil.template(template, {
      url: url,
      keys: JSON.stringify(getLocaKeysFromTemplate(file.contents)),
      file: file
    }));

    file.processedByLocaPrebuild = true;

    callback(null, file);

  };

}

function getLocaKeysFromTemplate(template) {
  var regexFilter = new RegExp("['\"]([^'\"]*)['\"]\\s*\\|\\s*loca", 'g');
  var matched = regexFilter.exec(template);
  var keys = [];
  while (matched && matched.length > 1) {
    var key = matched[1];
    keys.push(key);
    matched = regexFilter.exec(template);
  }
  return keys;
}

