/**
 * Created by bwong on 10/19/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('ExampleController', ExampleController);


  /* @ngInject */
  function ExampleController($log, loca) {
    var vm = this;
    vm.title = '';
    vm.datePickerIsOpen = '';
    vm.fetchedTranslation = null;

    vm.activate = activate;
    vm.openDatePicker = openDatePicker;

    vm.activate();


    ////////////////

    function fetchTranslationDemo() {
      loca.fetch('text.post_title_description.choosedevice')
        .then(function (text) {
          vm.fetchedTranslation = text;
        });
    }

    function activate() {
      vm.title = 'Example!';

      vm.text = {
        //default: 'default',
        match: 'matchme'
      };
      fetchTranslationDemo();
    }


    function openDatePicker(){
      $log.debug("opening");
      vm.datePickerIsOpen = true;
    }
  }

})();

