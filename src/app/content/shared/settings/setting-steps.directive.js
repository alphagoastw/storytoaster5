(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('settingSteps', settingSteps);

  /* @ngInject */
  function settingSteps() {
    var directive = {
      templateUrl:'app/content/shared/settings/setting-steps.html',
      scope: {
        steps: '=',
        currentStep: '=',
        onStepClicked: '&'
      },
      restrict: 'AE'
    };
    return directive;
  }


})();

