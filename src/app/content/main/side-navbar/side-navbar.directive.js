
(function(){
  'use strict';

  angular.module('storyToaster').directive('sideNavbar', sideNavbar);

  function sideNavbar(){
    var directive = {
      restrict: 'A',
      templateUrl: 'app/content/main/side-navbar/side-navbar.html',
      controller: 'MainController',
      controllerAs: 'MainController',
      bindToController: true
    };

    return directive;
  }

})();
