/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('HeaderController', HeaderController);


  /* @ngInject */
  function HeaderController($rootScope,
                            $log,
                            authService,
                            $state,
                            loginModalService,
                            $sessionStorage,
                            $window,
                            hideMenuItemsService) {
    var vm = this;

    $log = $log.withTag("HeaderController");
    vm.hideMenuService = hideMenuItemsService;
    vm.storage = $sessionStorage;
    vm.isLoggedIn = authService.isLoggedIn;

    vm.openLogin = openLogin;
    vm.logOut = logOut;
    vm.onHelpClicked = onHelpClicked;

    activate();

    //////////////////////////////////

    /**
     * Controller startup function
     */
    function activate() {
    }//end activate()

    function onHelpClicked() {
      var link = helpService.getLink();
      $log.debug('link:', link);
      if (link) {
        $window.open(link, '_blank');
      }
    }

    /**
     * Open modal dialog
     */
    function openLogin(){
      loginModalService.open();
    }//end openLogin()



    /**
     * Log Out user
     */
    function logOut(){
      $log.debug('header.logOut()');
      authService.logOut();
      $state.go('app.home');
    }//end logOut()
  }

})();

