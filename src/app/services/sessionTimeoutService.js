(function () {
  'use strict';

  angular.module('storyToaster')
    .factory('sessionTimeoutService', sessionTimeoutServiceFactory);

  /**
   * This empty run block is used to trigger the initialization of this service.
   * Don't remove it!
   */
  angular.module('storyToaster')
    .run(['sessionTimeoutService', function () {
    }]);

  function sessionTimeoutServiceFactory(config,
                                        $timeout,
                                        $rootScope,
                                        $log,
                                        authService,
                                        broadcastEvents,
                                        loginModalService,
                                        $state) {

    $log = $log.withTag('SessionTimeout');

    function SessionTimeout() {
      this.currentTimer = null;
      activate();
    }

    SessionTimeout.prototype.refreshTimer = refreshTimer;

    var instance = new SessionTimeout();

    return instance;

    function activate() {
      var self = instance;
      // On State change, refresh the timer.
      $rootScope.$on('$stateChangeStart', refreshTimer);
      $rootScope.$on(broadcastEvents.GLOBAL_LOGIN_CHANGED, refreshTimer);
    }

    function refreshTimer() {
      var self = instance;
      if (self.currentTimer) {
        $timeout.cancel(self.currentTimer);
        self.currentTimer = null;
      }
      if (authService.isAuthenticated()) {
        $log.debug("Timer is Refreshed!");
        self.currentTimer = $timeout(onTimeout, config.timeout);
      } else {
        // Not logged in, skip the timeout.
      }

    }

    function onTimeout() {
      $log.debug('Timeout!');
      var oldUserId = authService.getUserId();
      var currentState = $state.current;
      var params = $state.params;
      var callback = function () {
        if (authService.getUserId() === oldUserId) {
          $log.debug('Logged in with the same user. Go back to the previous state');
          $state.go(currentState, params);
        } else {
          $log.debug('Logged in with a different user account. Go through the new login process');
          $state.go('app.serviceAgreement');
        }
      };
      authService.logOut();
      $state.go('app.home');
      loginModalService.open(callback, {isTimeout: true});
    }
  }

})();
