(function(global) {

  "use strict";

  //fabric.CIText = fabric.util.createClass(fabric.IText, {
  //
  //  type: 'cIText',
  //  src: '',
  //
  //  initialize: function (text, options) {
  //    options || (options = {});
  //    this.callSuper('initialize', text, options);
  //
  //    var $itext = $('<textarea/>');
  //    $itext.addClass('itext');
  //
  //    this.on('editing:entered', function (e) {
  //      var obj = this;
  //
  //      obj.canvas.remove(obj);
  //      obj.canvas.renderAll();
  //
  //      $itext.css({
  //        left: obj.left + obj.canvas._offset.left,
  //        top: obj.top + obj.canvas._offset.top,
  //        width:obj.width,
  //        'resize':'both',
  //        //height:obj.height * 2,
  //        'line-height': obj.lineHeight,
  //        'font-family': obj.fontFamily,
  //        'font-size': Math.floor(obj.fontSize * Math.min(obj.scaleX, obj.scaleY)) + 'px',
  //        'font-weight': obj.fontWeight,
  //        'font-style': obj.fontStyle,
  //        color: obj.fill
  //      })
  //        .val(obj.text)
  //        .appendTo($(obj.canvas.wrapperEl).closest('#bookeditor'));
  //
  //      $itext.on('focusout', function(e) {
  //        obj.exitEditing();
  //        obj.set('text', $(this).val());
  //        $(this).remove();
  //        obj.canvas.add(obj);
  //        obj.canvas.renderAll();
  //      });
  //
  //    });
  //
  //    setTimeout(function() {
  //      $itext.select();
  //    }, 1);
  //  },
  //
  //  toObject: function () {
  //    return fabric.util.object.extend(this.callSuper('toObject'), {});
  //  }
  //
  //});

  //fabric.ImageTextbox = fabric.util.createClass(fabric.CIText, {
  fabric.ImageTextbox = fabric.util.createClass(fabric.IText, {

//var ImageTextbox = fabric.util.createClass(fabric.Textbox, {
///http://stackoverflow.com/questions/10009080/interactive-text-fields-with-fabric-js

    type: 'imageTextbox',
    src: '',

    id:'',
    flipH:false,
    flipV:false,

    initialize: function (text, options) {
      options || (options = {left:30, top:20});

      var thisTextObject = this;

      Object.getOwnPropertyNames(options).forEach(
              function (prop) {
                thisTextObject[prop] = options[prop];
                console.log(options[prop]);
              }
      );
      this.left = options.left ? options.left: 30;
      this.top = options.top ? options.top:20;

      this.callSuper('initialize', text);

      this.image = new Image();
      this.image.crossOrigin = "Anonymous";
      this.image.src = options.src;

      this.image.onload = (function () {
        this.loaded = true;
        this.setCoords();
        this.fire('image:loaded');
        if(this.canvas)
          this.canvas.renderAll();
      }).bind(this);
    },

    toObject: function () {
      return fabric.util.object.extend(this.callSuper('toObject'), {
        label: this.get('label'),
        src: this.image.src,
        id:this.id

      });
    },

    _render: function (ctx) {
      if (this.loaded) {
        //   ctx.drawImage(this.image, -this.width / 2 - 20, -this.height / 2 - 10, this.width + 40, this.height + 40);

        //this.flipH = true;
        //this.flipV = true;
        var scaleX = this.flipH ? -1 : 1, // Set horizontal scale to -1 if flip horizontal
          scaleY = this.flipV ? -1 : 1, // Set verical scale to -1 if flip vertical
          posX = this.flipH ? this.width * -1 : 0, // Set x position to -100% if flip horizontal
          posY = this.flipV ? this.height * -1 : 0; // Set y position to -100% if flip vertical

        if (this.flipH) {
          posX = this.flipH ? this.width / 2 * -1 : 0, // Set x position to -100% if flip horizontal
            posY = this.flipV ? this.height / 2 * -1 : 0; // Set y position to -100% if flip vertical
        }

        if(this.flipV){
          posY += this.height/2 + 10;
        }

        ctx.save(); // Save the current state
        ctx.scale(scaleX, scaleY); // Set scale to flip the image
        if (this.flipH) {
          ctx.drawImage(this.image, posX - 20, posY - this.height / 2 - 10, this.width + 40, this.height + 40); // draw the image
        }
        else {
          ctx.drawImage(this.image, posX - this.width / 2 - 20, posY - this.height / 2 - 10, this.width + 40, this.height + 40); // draw the image
        }

        //ctx.drawImage(img, posX -20, posY - 10, this.width + 40, this.height + 40); // draw the image
        ctx.restore(); // Restore the last saved state
        //ctx.drawImage(this.image, -this.width / 2 - 20, -this.height / 2 - 10, this.width + 40, this.height + 40);
      }

      this.callSuper('_render', ctx);
    }
  });

  fabric.ImageTextbox.fromObject = function (object) {
    var instance = new fabric.ImageTextbox(object.text, fabric.util.object.clone(object), function () {
      return instance && instance.canvas && instance.canvas.renderAll();
    });
    return instance;
  };

})( typeof exports != 'undefined' ? exports : this);
