/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('HomePageController', HomePageController);


  /* @ngInject */
  function HomePageController($log,
                              $state,
                              authService,
                              loginModalService) {
    $log = $log.withTag("HomePageController");

    var vm = this;
    vm.title = '';
    vm.newUser = {};
    vm.newUser.firstName = '';
    vm.newUser.lastName = '';
    vm.newUser.email = '';
    vm.newUser.emailConfirm = '';

    vm.openLogin = openLogin;

    activate();

    ////////////////

    function activate() {
      vm.title = 'HomePageController';

      if(authService.isAuthenticated()){
        $log.debug("Auth'd going to app.mybooks");
        $state.go('app.mybooks');
      }

    }//end activate()


    function openLogin(){
      loginModalService.open();
    }//end openLogin()



  }//end LoginController()

})();

