(function () {
  'use strict';
  angular.module('storyToaster').filter('format', formatFilterFactory);

  /* @ngInject */
  function formatFilterFactory() {
    /**
     * This is a C# style String.format filter.
     *
     * @returns {format}
     */
    function format(input) {
      var args = arguments;
      if (!input) {
        return input;
      } else {
        return input.replace(/\{(\d+)\}/g, function (match, capture) {
          return args[1 * capture + 1] || '';
        });
      }
    }

    return format;
  }
})();
