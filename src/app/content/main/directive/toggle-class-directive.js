/**
 * Created by Administrator on 2016/10/13.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('toggleClass', toggleClass);

  /* @ngInject */
  function toggleClass() {
    var directive = {
      restrict: 'A',
      link:link
    };
    return directive;

    function link(scope, element, attrs) {

      element.bind('click', function(evt) {
        element.toggleClass(attrs.toggleClass);
         updateCanvas(scope,evt);
      });
    }

    function updateCanvas(scope, evt){
      var canvas = scope.main.getCurrentCanvas();
      var p = evt.target.parentElement.id ;
      if(p == "font-size"){
        var fontSize = parseInt(evt.target.attributes['data-label'].value);
        $("#selected-font-size").attr('data-label',fontSize);

        var itext = canvas.getActiveObject();
        if(itext == null) return;
        scope.main.setTextSize(itext, fontSize);
        canvas.renderAll();
      } else if(p == 'text-color'){
        var textColor = evt.target.attributes['data-value'].value;
        var itext = canvas.getActiveObject();
        if(itext == null) return;
        scope.main.setTextColor(itext, textColor);
        canvas.renderAll();
      } else if(evt.target.tagName == 'IMG' && evt.target.parentElement.parentElement.id == 'text-font-type'){
        var textFontType = evt.target.parentElement.attributes['data-value'].value;
        $(evt.target).closest(".webfonts").find("#selected-font-type").text(textFontType);
        var itext = canvas.getActiveObject();
        if(itext == null) return;
        scope.main.setTextFontFamily(itext, textFontType);
        canvas.renderAll();
      }
    }
  }

})();


