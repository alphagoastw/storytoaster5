'use strict';
var path = require('path');
var gulp = require('gulp');
var shell = require('gulp-shell');
var args = require('yargs').argv;
var fs = require('fs');

var conf = require('./conf');

var validEnvs = ['dev','local','qa','qa3','production'];

var usage = function(){
  console.log('Usage:');
  console.log('gulp build --branch=[master|dv2cons3|production|qacons2|qa3|production] --version=version');
  console.log('gulp deploy --branch=[master|dv2cons3|production|qacons2|qa3|production] --version=version');
};

gulp.task('deployment', function () {

  // local is the default bulid target environment
  var env = 'local';
  if(args.env  && validEnvs.indexOf(args.env) >= 0){
    env = args.env;
  }

  // replace the correct config file before minify
  var configFile = 'index.constants.' + env + ".js";

  fs.createReadStream(path.join('config/',configFile))
    .pipe(fs.createWriteStream('src/app/index.constants.js'));
});

gulp.task('pre-build', function () {

  var branch = args.branch;

  return gulp.src("", {read: false})
    .pipe(shell([
      'echo Cleaning distribution folder...',
      'rm -fr dist'
    ]))
    .pipe(shell([
      'echo Preparing installation files...'
    ], {ignoreErrors: false, quiet: false, cwd: "."}))
    .pipe(shell([
      'git clone ssh://git@bitbucket.absolute.com:7999/buil/consumer-customer-center.git dist'
    ], {ignoreErrors: false, quiet: true, cwd: "."}))
    .pipe(shell([
      'git checkout ' + branch
    ], {ignoreErrors: false, quiet: true, cwd: "dist"}))
    .pipe(shell([
      'echo Cleaning distribution folder...',
      'rm dist/scripts/*',
      'rm dist/styles/*',
      'rm dist/index.html'
    ]))
});

gulp.task('push', function () {

  var branch = args.branch;
  var tag = args.version;
  var version = args.version.split('-')[0];
  fs.writeFileSync('./dist/version.txt',version);
  console.log('---- version changed! -----');

  return gulp.src("")
    .pipe(
    shell([
        'git add --all :/'
      ],{ignoreErrors: false, quiet: true, cwd:"./dist"}
    ))
    .pipe(
    shell([
        'git commit -m " ' + version + ' "',
        'git tag ' + tag + ' -f'
      ],{ignoreErrors: false, quiet: true,cwd:"./dist"}
    ))
    .pipe(
    shell([
        'git push origin ' + branch,
        'git push origin -f ' + tag
      ],{ignoreErrors: false, quiet: false,cwd:"./dist"}
    ))
});
