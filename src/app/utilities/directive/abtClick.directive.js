(function () {
  'use strict';

  angular.module('storyToaster').directive('abtClick', abtClickFactory);

  function abtClickFactory($log, $parse, $timeout) {
    $log = $log.withTag('abt-click');

    var directive = {
      link: link,
      restrict: 'A'
    };

    return directive;

    function link(scope, element, attrs) {
      if (attrs.ngBindHtml) {
        $log.debug('ngBingHtml detected. Add watcher to it');
        var ngBindHtmlWatch = $parse(attrs.ngBindHtml, function getStringValue(value) {
          return (value || '').toString();
        });
        scope.$watch(ngBindHtmlWatch, function () {
          $timeout(function () {
            // wait for the ngBindHtml finish its work.
            doLink(scope, element, attrs);
          });
        })
      } else {
        doLink(scope, element, attrs);
      }
    }

    function doLink(scope, element, attrs) {
      var clickTarget = attrs.abtClickTarget;
      if (!clickTarget) {
        $log.error('clickTarget is required for abtClick directive');
        return;
      }
      var callback = $parse(attrs.abtClick, /* interceptorFn */ null, /* expensiveChecks */ true);
      var target = element.find(clickTarget);
      if (target && target.length > 0) {
        // Remove default href link
        if (target.attr('href')) {
          target.attr('href', '');
        }
        target.on('click', function (event) {
          var fn = function () {
            callback(scope, {$event: event})
          };
          scope.$apply(fn);
          // To prevent old customer center href.
          if (angular.isFunction(event.preventDefault)) {
            event.preventDefault();
          } else {
            //event.returnValue = false;
          }
        });
      } else {
        $log.debug('No child node can be found based on the clickTarget:<' + clickTarget + '>');
      }
    }

  }
})();
