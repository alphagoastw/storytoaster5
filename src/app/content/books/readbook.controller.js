/**
 * Created by Administrator on 2015/11/7.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('ReadbookController', ReadbookController);

  /* @ngInject */
  function ReadbookController(
    $scope,
    $timeout,
    $window,
    bookRepository,
    $stateParams,
    bookSelected

  ) {
    var vm = this;

    vm.book = bookSelected;
    vm.addImage = addImage;

    $scope.book = bookSelected;
    $scope.pages = vm.book.pages;

    vm.bookId = $stateParams.id;

    $window.pageNumber = vm.book.pages.length;
    $scope.evenPageCount = vm.book.pages.length % 2 == 0;
    activate();

    var finishedPages = 0 ;
    var finishedOpenBookRender = false;

    $timeout( documentReady );

    $scope.$on('onPageAfterRender',function(){
      finishedPages = finishedPages + 1;
      console.log('finished pages=' +finishedPages);

      if(finishedOpenBookRender && vm.book && finishedPages >= vm.book.pages.length + 4 )
        documentReady();

    });

    $scope.$on('onOpenBookAfterRender', function(){

      console.log('onOpenBookAfterRender, finished pages=' +finishedPages);

      finishedOpenBookRender = true;

      if(finishedOpenBookRender && vm.book && finishedPages >= vm.book.pages.length + 4 )
      documentReady();
    });
    //angular.element(document).ready(documentReady);
    ////////////////

    function activate() {
      //
      //var bookId = $stateParams.bookId;
      //bookRepository.getBookById(bookId)
      //  .then(function (book) {
      //    vm.book = book;
      //  })
    }

    function addImage(){

    }

    function documentReady(){
      console.log('ready from readbook controller');
    }
  }

})();


