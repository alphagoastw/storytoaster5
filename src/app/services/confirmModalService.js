/**
 * Created by bwong on 2/24/16.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('confirmModalService', confirmModalService);


  /* @ngInject */
  function confirmModalService($log,
                               $q,
                               $modal) {
    $log = $log.withTag("confirmModalService");

    var service = {
      confirmActionModal: confirmActionModal
    };


    return service;

    ////////////////

    function confirmActionModal(headingTranslationKey, bodyTranslationKey) {

      var modalInstance = $modal.open({
        templateUrl: 'app/layouts/shared/modal/confirm-modal.html',
        controller: ModalController,
        controllerAs: 'vm',
        resolve: {
          modalHeader: function(){ return headingTranslationKey; },
          modalBody: function(){ return bodyTranslationKey; }
        }
      });


      return modalInstance.result;
    }//end confirmActionModal()
  }


  /* @ngInject */
  function ModalController(modalHeader, modalBody){
    var vm = this;
    vm.header = modalHeader;
    vm.body = modalBody;
  }//end ModalController()

})();

