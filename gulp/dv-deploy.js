var gulp = require('gulp');
var conf = require('./conf');
var path = require('path');

var $ = require('gulp-load-plugins')();

gulp.task('dv-deploy', ['build'], function () {
  return gulp.src(path.join(conf.paths.dist, '/**/*'))
    .pipe($.sftp(conf.sftp));
});
