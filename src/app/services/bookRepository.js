/**
 * Created by Brett Wang on 10/08/2015.
 */
'use strict';

angular.module('storyToaster')
  .service('bookRepository', function ($http, $q, $cookieStore, config, authService) {

    var self = this;
    var API_URL = config.apiRootPath;
    var currentUser = authService.currentUser();

    this.savePage = savePage;

    this.getBooks = function () {

      var dfd = $q.defer();
      self.promise = dfd.promise;
      $http.get(API_URL + '/books')
        .success(function (data, status) {
          console.log(data);
          dfd.resolve(data);
        }).error(function (data, status) {
          dfd.reject(status);
        });
      return self.promise;
    };

    this.getBookById = function(bookId){
      var dfd = $q.defer();
      self.promise = dfd.promise;

      var url = API_URL + 'books/' +bookId;

      $http.get(url).then(function (res) {
          var books = [];
          if (res.data) {
            var books = books.concat(res.data);
            books.forEach(function(book) {
              var pages = angular.fromJson(book.data);
              pages.forEach(function(page,index){
                book.pages[index] = page;
              });
            });
            if(books.length > 0)
              return dfd.resolve(books[0]);
            else
              return dfd.reject(null)
          }

          return dfd.reject(null);
        },
        function (err) {
          return dfd.reject(null);
        }).catch(function (err) {
          return dfd.reject(null);
        });

      return self.promise;
    };

    this.getUserBooks = function() {
      var dfd = $q.defer();
      self.promise = dfd.promise;
      var userId = currentUser ? currentUser.id : -1;

      $http.get(API_URL + 'users/' + userId + '/books').then(function (res) {
          var books = [];
          if (res.data) {
            books = books.concat(res.data);
            return dfd.resolve(books);
          }
          return dfd.resolve(books);
        },
        function (err) {
          return dfd.resolve([]);
        }).catch(function (err) {
          return dfd.resolve([]);
        });

      return self.promise;
    };

    this.saveToServer = function(book){
      this.author = currentUser ? currentUser.id : -1;
      var obj = angular.copy(book);

      // delete previewImage
      obj.pages.forEach(function(page){
        delete page.previewImage;
      });

      delete obj.frontCover.previewImage;
      delete obj.dedicatedPage.previewImage;
      delete obj.backCover.previewImage;
      delete obj.leftDesignPage;
      delete obj.rightDesignPage;

      //var fc = new fabric.Canvas(
      //  'frontCoverCanvas',
      //  {
      //    backgroundColor: obj.backgroundColor
      //  }
      //);
      //fc.loadFromJSON(obj.frontCover.imageData,
      //  function(){
      //    var objects = fc.getObjects();
      //    console.log('all object', objects);
      //    objects.forEach(function(o){
      //      if(o.type == 'image'){
      //        o.src=null;
      //      }
      //    })
      //
      //    console.log(JSON.stringify(fc));
      //  }
      //);


      obj.data = JSON.stringify(obj.pages);
      //obj.data = obj.pages;
      var promise;

      if (!book.id || book.id < 0) {
        promise = createOneBook(obj);
      }
      else {
        promise = updateBook(obj);
      }

      promise
      .then(function (res) {
          book.id  = res.data.id;
          return res.data;
        });

      return promise
    };

    this.deleteBook = function(book){
      var url = API_URL + "books/" + book.id;

      var dfd = $q.defer();
      $http.delete(url)
        .then(function(res){
          console.log(res);
          dfd.resolve(res);
        });

      return dfd.promise;
    };

    function updateBook(book) {
      var url = API_URL + "books/" + book.id;
      return $http.put(url,book)
    }

    function createOneBook (book){
      var url = API_URL + "books";
      return $http.post(url,book);
    }

    function savePage(bookId, page) {
      return;

      if (!page.id) {
        // create a new page;
        console.log('create a new page');
        createBookPage(bookId, page.imageData).
          then(
          function(res){
            console.log('page created:', res.data);
            page.id = res.data.id ;
          },

          function(err){
            console.log('create book page fails, error=', err);
          }
        )
      }
      else {
        console.log('update the existing page');
        updateBookPage(bookId, page.Id, page.imageData);
      }
    }

    function createBookPage(bookId, pageInfo){
      var url = API_URL + "books/" + bookId + "/pages";
      $http.post(pageInfo);
    }

    function updateBookPage(bookId,pageId, pageInfo){
      var url = API_URL + "books/" + bookId + "/pages/" + pageId;

      $http.put(pageInfo);
    }

    function getBookPages(bookId){
      var url = API_URL + "books/" + bookId;
      $http.get(url);
    }

  });

