(function(){
  'use strict';
  var PREFIX = 'com.absolute.ccc.';

  angular.module('storyToaster')
    .constant('broadcastEvents',{
      /** Fired when locale of application is changed. Value: newLocale string */
      GLOBAL_LOCALE_CHANGED: PREFIX+'global_locale_changed',
      /** Fired when login status is changed. Value: user entity if logged in, or null if logged out */
      GLOBAL_LOGIN_CHANGED: PREFIX + 'global_login_changed',
      GLOBAL_TIMEOUT: PREFIX + 'global_timeout'
    });
})();
