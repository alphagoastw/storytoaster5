(function () {
  'use strict';

  angular.module('storyToaster')
    .factory('loginModalService', loginModalServiceFactory);

  function loginModalServiceFactory($log,
                                    $modal,
                                    $state) {
    var handlerStack = [defaultHandler];

    function LoginModalService() {

    }

    LoginModalService.prototype.open = open;
    LoginModalService.prototype.registerHandler = registerHandler;
    LoginModalService.prototype.unregisterHandler = unregisterHandler;

    return new LoginModalService();

    ///////////////////////////////////////

    /**
     *
     * @param handler
     */
    function registerHandler(handler) {
      var index = handlerStack.indexOf(handler);
      if (index >= 0) {
        $log.error('handler is already in stack!');
      } else {
        handlerStack.push(handler);
      }
    }

    function unregisterHandler(handler) {
      var index = handlerStack.indexOf(handler);
      if (index >= 0) {
        handlerStack.splice(index, 1);
      } else {
        $log.error('handler is not found is stack');
      }
    }

    /**
     * The default handler, which redirects user to the service agreement page.
     */
    function defaultHandler() {
      $log.debug('Login Success is handled by default handler');
      $state.go('app.mybooks');
    }

    /**
     * Open the login modal
     * @param handler {Function}
     *     Optional. If set, the handler will be used as the login success callback. If omitted, use the registered handler in stack.
     */
    function open(handler, options) {
      if (!angular.isFunction(handler)) {
        handler = handlerStack[handlerStack.length - 1];
      }
      return $modal.open({
        templateUrl: 'app/content/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login',
        resolve: {
          onLoginSucceed: function () {
            return handler;
          },
          modalOptions: function () {
            return options;
          }
        }
      });
    }

  }

})();
