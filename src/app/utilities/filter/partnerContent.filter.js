(function () {
  'use strict';
  
  angular.module('storyToaster').filter('partnerContent', partnerContentFactory);

  /**
   * This filter is created to perform backward compact on
   */
  function partnerContentFactory(config) {
    function filter(input) {
      var base = config.partnerContentPath;
      return input.replace('~', base);
    }

    return filter;
  }
})();
