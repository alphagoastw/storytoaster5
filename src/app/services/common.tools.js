(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('commonTools', commonTools);

  /* @ngInject */
  function commonTools(deviceDetector) {
    var service = {
      fixTextTooLong: fixTextTooLong,
      getLocalizedDate: getLocalizedDate,
      toApiError: toApiError
    };
    return service;

    ////////////////

    /**
     * try to parse the api error response to a ng-message object.
     * @param err
     * @param ie10Hack
     *    IE10 has an annoying issue with 401/403 responses.
     *    If this parameter is set, this method will use this value as the status code in IE10.
     */
    function toApiError(err, ie10Hack) {
      var status = 500;
      if (err.status) {
        status = err.status;
      }
      /* IE 10 has a known issue with 401/403 responses. */
      if (ie10Hack && status === -1 &&
        deviceDetector.browser === 'ie' &&
        deviceDetector['browser_version'] === "10.0") {
        status = ie10Hack;
      }
      var error = {};
      switch (status) {
        case 400:
          error['error400'] = true;
          break;
        case 401:
          error['error401'] = true;
          break;
        case 403:
          error['error403'] = true;
          break;
        case 404:
          error['error404'] = true;
          break;
        case 500:
        default:
          error['error500'] = true;
          break;
      }
      return error;
    }

    /**
     *
     * @param text
     * @param leng
     * @returns {*}
     */
    function fixTextTooLong(text, leng){
      if (text  && text.length > leng)
        return text.substring(0, leng - 2) + "...";

      return text ? text: '';
    }//end fixTextTooLong()

    /**
     *
     * @param date
     * @returns {string}
     */
    function getLocalizedDate(date){
      var dt = Date.parse(date);
      if(isNaN(dt)){
        return 'N/A';
      }

      var utcDate = new Date(date);
      return utcDate.toLocaleDateString();
    }//end getLocalizedDate()



  }

})();

