(function () {
  'use strict';

  /**
   * This file will NOT be overridden by gulp build.
   * It's a good place to put common constants.
   */
  angular
    .module('storyToaster')
    .constant('moment', moment);
})();
