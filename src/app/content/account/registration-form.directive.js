(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('registrationForm', registrationForm);

  registrationForm.$inject = [];

  /* @ngInject */
  function registrationForm() {
    var directive = {
      bindToController: true,
      controller: 'AccountController',
      controllerAs: 'account',
      restrict: 'AE',
      templateUrl:'app/content/account/registration-form.html',
      scope:{
        nextstep: '&?'
      }
    };
    return directive;
 
  } //end registrationForm()

})();


