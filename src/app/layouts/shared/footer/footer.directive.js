/**
 * Created by bwong on 10/13/15.
 */
(function () {
  'use strict';

  angular.module('storyToaster')
    .directive('footer', footer);


  /* @ngInject */
  function footer() {
    var directive = {
      bindToController: true,
      controller: 'FooterController',
      controllerAs: 'footer',
      templateUrl: 'app/layouts/shared/footer/footer.html',
      restrict: 'A',
      scope: {}
    };
    return directive;


  }//end footer


})();

