/**
 * Created by bwong on 10/29/15.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('matchField', matchField);

  matchField.$inject = ['$log'];

  /* @ngInject */
  function matchField() {
    var directive = {
      link: link,
      require: 'ngModel',
      restrict: 'A',
      scope: {
        matchTo: '=with'
      }
    };
    return directive;

    function link(scope, element, attrs, ctrl) {
      scope.$watchCollection('matchTo', function () {
        ctrl.$validate();
      });

      ctrl.$validators.matchingField = function doesFieldMatch(modelValue){
        //$log.debug("matchTo:", scope.matchTo.$viewValue);
        if(scope.matchTo.$valid && scope.matchTo.$dirty){
          return (scope.matchTo.$viewValue === modelValue);
        } else {
          return false;
        }
      };

    }//end link()

  }//end matchField()

})();

