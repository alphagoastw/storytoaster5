(function() {
  'use strict';

  angular
    .module('storyToaster')
    .config(config);

  /** @ngInject */
  function config($logProvider,
                  cfpLoadingBarProvider,
                  $locationProvider,
                  $localStorageProvider,
                  $sessionStorageProvider,
                  uiGmapGoogleMapApiProvider,
                  toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    //toastr.options.timeOut = 3000;
    //toastr.options.positionClass = 'toast-top-right';
    //toastr.options.preventDuplicates = true;
    //toastr.options.progressBar = true;

    //Angular-Toastr
    angular.extend(toastrConfig, {
      positionClass: 'toast-bottom-right'
    });

    //Angular Loading Bar Config
    cfpLoadingBarProvider.includeSpinner = true;

    $locationProvider.html5Mode({
      enabled: true
    });

    var prefix = 'com.absolute.ccc.';
    // ngStorage config
    $sessionStorageProvider.setKeyPrefix(prefix);
    $localStorageProvider.setKeyPrefix(prefix);

    // Google Maps Config
    uiGmapGoogleMapApiProvider.configure({
      client: 'gme-absolutesoftware'
    });

  }//end config()
})();
