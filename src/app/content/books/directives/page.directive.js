/**
 * Created by Administrator on 2015/11/7.
 */
(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('bookPage', bookPage);

  bookPage.$inject = [];

  /* @ngInject */
  function bookPage() {
    var directive = {
      templateUrl: 'app/content/books/templates/page.html',
      //bindToController: true,
      //controller: 'ReadbookController',
      //controllerAs: 'vm',
      link: link,
      restrict: 'AE',
      scope: {
        'data': '=data',
        'id':'=id',
        'width':'=width',
        'height':'=height',
        'imageWidth' :'=imageWidth',
        'imageHeight' : '=imageHeight'
      }
    };
    return directive;

    function link(scope, element, attrs) {

      var data = scope.data;
      var id = scope.id ;
      var width = scope.width;
      var height = scope.height;

      var imageWidth = width;
      var imageHeight = height;

      if(scope.imageWidth){
        imageWidth = scope.imageWidth;
      }

      if(scope.imageHeight){
        imageHeight = scope.imageHeight;
      }

      var xscale = width / imageWidth;
      var yscale = height / imageHeight;

      if(!scope.imageWidth){
        scope.imageWidth = 1;
      }

      if(!scope.imageHeight){
        scope.imageHeight = 1;
      }

      var canvasEle = element.find('canvas')[0];
      var id = "canvas_" + id;
      canvasEle.id = id;

      //set canvasEle size
      canvasEle.width = width;
      canvasEle.height = height;

      var canvas = new fabric.Canvas(
        id,
        {select:false,background:'red'}
      );
      //scope.vm.canvas = canvas;
      scope.canvas = canvas;
      if(data && data.imageData)
        canvas.loadFromJSON(data.imageData, canvas.renderAll.bind(canvas), function (o, object) {

          object.selectable = false;
          //
          //object.lockRotation = true;
          //object.hasBorders = false;
          //object.lockMovementX = object.lockMovementY = true;
          //object.selection = false;
          //object.hasControls = false;

          object.left = object.left * xscale;
          object.width = object.width *  xscale;
          object.height = object.height * yscale;
          object.top = object.top * yscale;

          scope.$emit('onPageAfterRender');
      });
      else{
        scope.$emit('onPageAfterRender');
      }
    }
  }

})();

