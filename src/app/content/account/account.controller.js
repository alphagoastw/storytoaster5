(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('AccountController', AccountController);

  /* @ngInject */
  function AccountController($scope,
                             authService,
                             $state,
                             broadcastEvents,
                             $q,
                             $log,
                             commonTools) {
    var vm = this;
    var usaStateList = [];
    var canadaStateList = [];

    vm.createAccount = createAccount;
    activate();

    // Implementation


    ////////////////

    function activate() {
    }


    function createAccount(formValid) {

      $log.debug(formValid.firstName.$invalid);

      if ( !vm.user.email || !vm.user.confirmEmail ||
        vm.user.email !== vm.user.confirmEmail || !vm.user.password || vm.user.password !== vm.user.confirmPassword) {

        // show message of email is not correct;
        $log.debug('data error');
        return -1;
      }

      authService.register(vm.user)
        .then(function (user) {
          // succeed create an new
          vm.registrationError = null;
          $log.debug(user);
          if (vm.nextstep) {
            vm.nextstep();
          } else {
            $state.go('app.device.list');
          }
        })
        .catch(function (err) {
          $log.debug(err);
          vm.registrationError = commonTools.toApiError(err);
          if (err.status == 400) {
            //message "Log in already exists,Please create a new log in."  when an email has already there
            //message code : 1099
            // show the message
            //formValid.email.$invalid = true;
            vm.emailExists = true;
          }
        });
    }

  }

})();


