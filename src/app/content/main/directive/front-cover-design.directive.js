(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('frontCoverDesign', frontCoverDesign);


  /* @ngInject */
  function frontCoverDesign() {

    var page;
    var canvas;

    var t = ($('.editor')).width();
    var w = (t - 40) / 2;
    var h = w / 1.375;

    var directive = {
      link: link,
      restrict: 'AE',
      //template: template,
      templateUrl: 'app/content/main/template/front-cover-design.html'
    };
    return directive;
    /* @ngInject */
    function link(scope,  elem, attrs) {

      page = scope.main.PhotoBook.frontCover;

      var picture = {};
      picture.width = w * 0.6;
      picture.height = h * 0.6;
      picture.x = (w - picture.width ) / 2;
      picture.y = (h - picture.height) / 2;

      var canvasWidth = w - 10;
      var canvasHeight = h - 10;

      scope.main.PhotoBook.frontCover.width = canvasWidth;
      scope.main.PhotoBook.frontCover.height = canvasHeight;

      canvas = new fabric.Canvas(
        'frontCoverCanvas',
        {
          selection: true,
          backgroundColor: scope.main.PhotoBook.backgroundColor
        }
      );

      canvas.setWidth(canvasWidth);
      canvas.setHeight(canvasHeight);

      scope.main.setFrontCanvas(canvas);
      scope.main.currentCanvas = canvas;

      if (!scope.main.PhotoBook.id) {
        //set title and attribute
        var title = addBookTitle(scope);
        var attribute = addAttribute(scope, picture);

        canvas.on("text:changed", function (e) {
          if (e.target.id == title.id) {
            scope.main.PhotoBook.title = e.target.text;
          } else if (e.target.id == attribute.id) {
            scope.main.PhotoBook.author = e.target.text;
          }
        });

        picture.url = "http://localhost:3000/assets/images/blank.jpg";

        //set image
        addPicture(scope, picture);
      }

      //set monitor
      addMonitors(scope, title, attribute, picture, canvasWidth, canvasHeight);
      fireChangeEvent(scope);

      scope.$emit('onAfterRenderForFrontCover');


      function addAttribute(scope, picture) {

        var top = picture.y + picture.height + 20;

        var attribute = new fabric.StoryToasterIText(scope.main.PhotoBook.attribute, {
          fontSize: 20,
          fontFamily: 'Comic Sans',
          fontWeight: 'bold',
          lockRotation: true,
          lockMovement: true,
          selection: true,
          hasControls: true,
          hasBorders: true,
          cornersize: 0,
          originX: 'center',
          top: top,
          id: 'author'
          //left: 0
        });
        attribute.on("changed", function (e) {
          scope.main.PhotoBook.author = attribute.text;
        });

        attribute.selectable = true;
        canvas.add(attribute);
        attribute.centerH();
        attribute.bringToFront();
        attribute.setCoords();
        fireChangeEvent(scope);

        return attribute;
      }

      function addBookTitle(scope) {

        var title = new fabric.StoryToasterIText(scope.main.PhotoBook.title, {
          fontSize: 30,
          fontFamily: 'Comic Sans',
          fontWeight: 'bold',
          lockRotation: true,
          lockMovement: true,
          selection: true,
          hasControls: true,
          hasBorders: true,
          cornersize: 0,
          //originX: 'center',
          top: 30,
          id: 'bookTitle'
        });

        title.on("text:changed", function (e) {
          scope.main.PhotoBook.title = title.text;
        });

        title.selectable = true;
        canvas.add(title);
        title.centerH();
        title.setCoords();
        fireChangeEvent(scope);

        return title;
      }

      function addPicture(scope, picture, canvasWidth, canvasHeight) {

        if (!picture.url) return;

        fabric.Image.fromURL(picture.url, function (img) {

          img.selectable = false;

          canvas.add(img.set({
            left: picture.x,
            top: picture.y,
            width: picture.width,
            height: picture.height,

            hasControls: false,
            //cornerColor: 'green',cornerSize: 16,transparentCorners: false,
            selection: false,
            lockRotation: true,
            lockMovement: true,   //lockMovementY: false,lockMovementX: false,
            //lockUniScaling: false,lockScalingY:false, lockScalingX:false,
            hoverCursor: 'default',
            hasRotatingPoint: false,
            hasBorders: true, borderColor: 'white', borderSize: 2,
            transparentBorder: false,
            angle: 0,
            cornersize: 10
          }));

          //canvas.setActiveObject(canvas.item(2));
          //canvas.item(2).selectable = false;
          img.bringToFront();
          canvas.hideToolItems();

          fireChangeEvent(scope);

        }, {crossOrigin: 'Anonymous'});
      }

      function fireChangeEvent(scope) {
        scope.$emit('pageChanged', {canvas: canvas, page: page});
      }

      function addMonitors(scope, title, attribute, picture, canvasWidth, canvasHeight) {

        scope.$on("frontCoverOpened", function (event, args) {
          canvas.clear();
          canvas.backgroundColor = scope.main.PhotoBook.backgroundColor;
          canvas.loadFromJSON(scope.main.PhotoBook.frontCover.imageData,
            function () {
              scope.main.PhotoBook.frontCover.previewImage = canvas.toDataURL();
              canvas.renderAll();
            })
        });

        scope.$watch(
          function () {
            return scope.main.PhotoBook.backgroundColor;
          },

          function (newValue, oldValue) {
            canvas.backgroundColor = newValue;
            canvas.setBackgroundColor(newValue);
            canvas.renderAll();
            fireChangeEvent(scope);
          });

        scope.$watch(
          function () {
            return scope.main.PhotoBook.frontCoverImageIndex;
          },

          function (newValue, oldValue) {

            if (newValue >= 0) {
              if (canvas.item(2))
                canvas.item(2).remove();

              canvas.setWidth(canvasWidth);
              canvas.setHeight(canvasHeight);

              if (scope.main.PhotoBook.pages.length > newValue) {
                picture.url = scope.main.PhotoBook.pages[newValue].previewImage;
                addPicture(scope, picture, canvasWidth, canvasHeight);
              }
            }
          }
        )
      }
    }
  }

})();




