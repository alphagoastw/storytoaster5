

(function(){
  'use strict';
  angular
    .module('storyToaster')
    .directive('backCoverDesignRight', backCoverDesignRight);

  /* @ngInject */
  function backCoverDesignRight() {
    return {
      templateUrl: 'app/content/main/template/back-cover-design-right.html'
    }
  }
})();
