(function () {
  'use strict';

  angular
    .module('storyToaster')
    .factory('registrationService', registrationService);


  /* @ngInject */
  function registrationService($http,
                               $q,
                               config,
                               authService) {

    var API_URL = config.apiRootPath;

    var service = {
      getRegistrationEvents: getRegistrationEvents,
      getFailedEvents:getFailedEvents,
      abandonFailedInstall: abandonFailedInstall,
      createRegistrationEvent: createRegistrationEvent
    };

    return service;

    ////////////////

    function abandonFailedInstall(regCode, customerId) {
      customerId = customerId || authService.getUserId();

      return getFailedEvents(customerId)
        .then(function (failedEvents) {
          var promise = null;
          if (angular.isArray(failedEvents)) {
            failedEvents.some(function (action) {
              if (regCode === action.regcode) {
                var evt = {
                  customerId: customerId,
                  regcode: regCode,
                  action: 'Abandoned',
                  installerGuid: action.installerGuid
                };
                promise = createRegistrationEvent(evt);
                return true;
              } else {
                return false;
              }
            });
          }
          if (promise) {
            return promise;
          }
          else {
            return $q.resolve();
          }
        });
    }

    function createRegistrationEvent(evnt){
      var url = API_URL + "customer/"+evnt.customerId +"/registrationEvents";

      return $http.put(url, evnt);
    }

    function getRegistrationEvents(customerId){
      var url = API_URL + "customer/" + customerId + "/registrationEvents";
      return $http.get(url)
        .then(function (res) {
          return res.data;
        });
    }

    function getFailedEvents(customerId){
      return getRegistrationEvents(customerId)
        .then(function (data) {
          return filterFailedEvents(data);
        });
    }

    //////////////////////
    function filterFailedEvents(events) {
      var eventsByRegCode = _.chain(events).groupBy("regcode").sortBy('createdAt').value();
      if (!eventsByRegCode || eventsByRegCode.length < 1) {
        return;
      }

      // The goal is to get the latest failed event, only if there's no abandoned event after that.
      return eventsByRegCode.reduce(function (failedEvents, events) {
        for (var i = eventsByRegCode.length - 1; i >= 0; i--) {
          var event = eventsByRegCode[i];
          if (event.active === 'Abandoned' || event.active === 'Installed') {
            // Has an abandoned event or installed event before any failed event. return null;
            break;
          } else if (event.action === 'InstallFailed') {
            failedEvents.push(event);
            break;
          }
        }
        return failedEvents;
      }, []);
    }
  }

})();

