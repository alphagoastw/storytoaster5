(function () {
  'use strict';

  angular
    .module('storyToaster')
    .directive('textToolbar', textToolbar);

  /* @ngInject */
  function textToolbar() {
    var directive = {
      link: link,
      restrict: 'A',
      templateUrl:'app/content/main/template/text-tool-bar-2.html'

    };
    return directive;

    function link(scope, element, attrs) {

      // Enable all tooltips
      $('[data-toggle="tooltip"]').tooltip();

      // Can control programmatically too
      $('.ql-italic').mouseover();
      setTimeout(function() {
        $('.ql-italic').mouseout();
      }, 2500);
    }
  }

})();


