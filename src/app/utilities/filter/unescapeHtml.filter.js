(function () {
  'use strict';

  angular.module('storyToaster').filter('unescapeHtml', unescapeHtmlFactory);

  function unescapeHtmlFactory() {
    function filter(input) {
      if (!input) {
        return null;
      }
      var e = document.createElement('div');
      e.innerHTML = input;
      return e.childNodes.length === 0 ? null : e.childNodes[0].nodeValue;
    }

    return filter;
  }
})();
