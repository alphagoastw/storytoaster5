(function () {
  'use strict';

  angular
    .module('storyToaster')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout,
                          relayService,
                          $scope,
                          $rootScope,
                          $state,
                          $window,
                          //$routeParams,
                          $stateParams,
                          imageService,
                          PhotoBook,
                          bookRepository,
                          config,
                          fabricJSExt) {
    console.log('----------- in main controller ---------------');

    //console.log($routeParams.bookid);

    var vm = this;
    var scale;
    //vm.bookId = $routeParams.bookid;
    vm.bookId = $stateParams.bookId;
    if(vm.bookId){
      vm.editBook = true;
    }

    vm.PhotoBook = new PhotoBook();

    if($rootScope.deviceDetector.device == 'ipad'){
      vm.PhotoBook.pagesInDesign = 1;
    } else {
      vm.PhotoBook.pagesInDesign = 2;
    }

    vm.contentPageMode = true;
    vm.currentPage = vm.PhotoBook.pages[0];
    vm.currentPage.active = true;

    vm.currentCanvas = vm.left_canvas;

    function restoreBoook(bookdId) {

      if (!vm.bookId && vm.bookId != "") {
        vm.left_canvas.clear();
        vm.right_canvas.clear();
        vm.PhotoBook.fonrtCoverImageIndex = -1;
        return;
      }

      bookRepository.getBookById(vm.bookId)
        .then(function (book) {

          if (!book) {
            vm.left_canvas.clear();
            vm.right_canvas.clear();
            vm.PhotoBook.fonrtCoverImageIndex = -1;
            return;
          }
          vm.selectedBook = book;

          vm.PhotoBook.pages = book.pages;
          $timeout(function() {
            Object.getOwnPropertyNames(book).forEach(
              function (prop) {
                vm.PhotoBook[prop] = book[prop] || vm.PhotoBook[prop];
              }
            );

            vm.PhotoBook.pages = angular.fromJson(vm.PhotoBook.data);
            vm.PhotoBook.frontCover =book.frontCover;
            $rootScope.$broadcast('frontCoverOpened',
              {
                imageData:book.frontCover.imageData,
                backgroundColor:book.backgroundColor
              }
            );
            $rootScope.$broadcast('backCoverOpened');

            var contentWidth = book.frontCover.width;

            vm.PhotoBook.pages.forEach(function(page){
              if(!vm.recoverCanvasArray) return;

              var rc = vm.recoverCanvasArray[page.index];
              var w = rc.width ;
              if(!scale){
                scale = (w -10) / contentWidth;
              }

              rc.loadFromJSON(page.imageData,
                function(){
                   page.previewImage = rc.toDataURL();
                   if(page.index == book.frontCoverImageIndex){
                    vm.PhotoBook.frontCoverImageIndex = -1;
                    $timeout(function(){
                      vm.PhotoBook.frontCoverImageIndex =page.index ;
                    },2000)
                  } else if(page.index == book.backCoverImageIndex){
                    vm.PhotoBook.backCoverImageIndex = -1;
                    $timeout(function(){
                      vm.PhotoBook.backCoverImageIndex =page.index ;
                    },2000)
                  }
                });
              rc.setZoom(scale);
              frontCoverClick();
            });
            restoreCanvasData(vm.frontCoverCanvas,vm.PhotoBook.frontCover.imageData, vm.frontCoverCanvas);
            restoreCanvasData(vm.backCoverCanvas,vm.PhotoBook.backCover.imageData, vm.backCoverCanvas);
            //vm.frontCoverCanvas.setZoom(1);
          } );

        })
    }

    activate();

    angular.element(document).ready(documentReady);

    //---------------------------------------------------- methods
    vm.clickOnTool = clickOnTool;
    vm.removeBackground = removeBackground;
    vm.addNoBgStoryText = addNoBgStoryText;
    vm.clickBook = clickBook;
    vm.changeTitleFont = changeTitleFont;
    vm.changeTitleColor = changeTitleColor;
    vm.changeAuthorFont = changeAuthorFont;
    vm.changeAuthorColor = changeAuthorColor;
    vm.changeBackgroundColor = changeBackgroundColor;
    vm.readBook = readBook;
    vm.coverImageSelected = coverImageSelected;
    vm.backCoverImageSelected = backCoverImageSelected;
    vm.selectLeft = selectLeft;
    vm.selectRight = selectRight;
    vm.addImage = addImage;
    vm.addText = addText;
    vm.frontCoverClick = frontCoverClick;
    vm.dedicatedPageClick = dedicatedPageClick;
    vm.backCoverClick = backCoverClick;
    vm.previewClick = previewClick;
    vm.nextPage = nextPage;
    vm.previousPage = previousPage;
    vm.drawMode = drawMode;
    vm.backCurrentDesignData = backCurrentDesignData;
    vm.restoreToCurrentDesignData = restoreToCurrentDesignData;
    vm.generatePreviewImage = generatePreviewImage;
    vm.deleteObject = deleteObject;
    vm.newPage = newPage;
    vm.copyPage = copyPage;
    vm.deletePage = deletePage;
    vm.saveToServe = saveToServe;
    vm.finishCreateBook = finishCreateBook;
    vm.nextSaveToImage = nextSaveToImage;
    vm.setFrontCanvas = setFrontCanvas;
    vm.getCurrentCanvas = getCurrentCanvas;
    vm.setTextColor = setTextColor;
    vm.setTextFontFamily = setTextFamily;
    vm.setTextSize = setTextSize;
    vm.setRecoverCanvasArray = setRecoverCanvasArray;
//---------------------------------------------------- methods end

function setRecoverCanvasArray(arr){
  vm.recoverCanvasArray  = arr;
  restoreBoook();
}
//---------------------------------------------------- test data start
    $scope.test = 'this is a test ';

    $scope.customSettings = {
      control: 'brightness',
      theme: 'bootstrap',
      position: 'top left',

    };
    $scope.inlinesettings = {
      inline: true
    };
    $scope.brightnesssettings = {
      control: 'brightness'
    };

// JS ngjsColorPicker
    $scope.optionsColumn = {
      columns: 8,
      roundCorners: true,
      size: 30
    };

    $scope.colours = [{
      name: "Red",
      hex: "#F21B1B"
    }, {
      name: "Blue",
      hex: "#1B66F2"
    }, {
      name: "Green",
      hex: "#07BA16"
    }];

    $scope.colour = "";

    function addNoBgStoryText(){
      if(vm.currentCanvas) {
        vm.currentCanvas.addStoryText("");
      }
    }
    function addText(imageUrl) {
      if(vm.currentCanvas)
         vm.currentCanvas.addText(imageUrl);
    }

    function addStoryText(imageUrl){
      //imageUrl = "";
      if(vm.currentCanvas) {
        vm.currentCanvas.addStoryText(imageUrl);
      }
    }

//------------------------------------------------------ test data ends

//------------------------------------------------------ listen events

    $scope.$on('onAfterRenderForFrontCover',function(){
    //  console.log('onAfterRenderForFrontCover');
    });

    $scope.$on('canvasRenderAfter',function(){
      //console.log('canvas object render finished ');
      //generatePreviewImage();
    });

    $scope.$on('onAfterRender',function(){
      //console.log('after render for covers');

    });

    $scope.$on('designCanvasInitialed',function(){
    });

    $scope.$on('saveBook',function(){
      saveToServe();
    });

    $scope.$on("savePage",function(event,args){
      var page = args;
      console.log('page info =', page);
      bookRepository.savePage(vm.PhotoBook.id,page);
    });

    $scope.$on('$viewContentLoaded', documentReady);

    $scope.$on('pageChanged', function (event, args) {
    });

    $scope.$on('addImage', function (event, args) {

      if (vm.PhotoBook.pages.length < 1) return;
      if (vm.PhotoBook.leftDesignPage && vm.PhotoBook.leftDesignPage.active)
        vm.currentCanvas = vm.left_canvas;
      else if (vm.PhotoBook.rightDesignPage) {
        vm.currentCanvas = vm.right_canvas;
      }

      var imageUrl = args.imageUrl;
      var operation = args.operation;
      if (operation === 'background') {
        //add for backgroupd
        vm.addImage(imageUrl, true);
      }
      else if (operation === 'props' ) {
        vm.addImage(imageUrl);
      } else if(operation  === 'text'){
        vm.addText(imageUrl);
      } else if(operation ==='bigText'){
        imageUrl = imageUrl + "?size=origin";
        addStoryText(imageUrl);
      //  vm.addText(imageUrl);
      }

      generatePreviewImage();
    });
//------------------------------------------------------ listen events end

    //------------------------------------------------------ font list

    vm.languages =[
      {
        name:'Arial',
        value:'Arial'
      },
      {
        name:'Times New Roman',
        value:'Times New Roman'
      }
    ];


    //------------------------------------------------------ font list ends
    function activate() {


      imageService.getBackgroundImages()
        .then(
        function (data) {
          $scope.backgroundGroup = data;
          $scope.backgroundGroupText ='背景素材';
        });

      imageService.getPropsImages().then(
        function (data) {
          $scope.propsGroup = data;
          $scope.propsGroupText ='故事素材';
        });

      imageService.getTextImages().then(
        function (data) {
          $scope.textImages = data;
          $scope.textGroupText ='对话泡泡';
        });

      imageService.getBigTextImages().then(
        function (data) {
          $scope.bigTextImages = data;
          $scope.bigTextGroupText='故事文字';
      });

      $scope.groups = imageService.getImages();

      fabricJSExt.init();
    }


//------------------------------------------------------ tool items start

    function clickOnTool(event) {
      //if (!vm.currentCanvas) return;
      var c = getCurrentCanvas();
      if(c)
      var activeObj = c.getActiveObject();

      if (!activeObj) return;
      if (event === 'delete') {
        activeObj.remove();
      } else if (event === 'copy') {
        activeObj.copy();
      } else if (event === 'bringToFront') {
        activeObj.bringForward();
      } else if (event === 'sendToBack') {
        activeObj.sendBackwards();
      } else if (event === 'flipX') {
        activeObj.flipByX();
      } else if (event === 'flipY') {
        activeObj.flipByY();
      } else if (event === 'font-bold') {
        setTextBold(activeObj);
        //if (activeObj.fontWeight === 'bold') {
        //  activeObj.fontWeight = 'normal'
        //} else {
        //  activeObj.fontWeight = 'bold';
        //}
      } else if (event === 'font-italic'){
        console.log(activeObj.fontStyle);
        setTextItalic(activeObj);
        //if(activeObj.fontStyle ==='italic'){
        //  activeObj.fontStyle = 'normal'
        //} else {
        //  activeObj.fontStyle = 'italic';
        //}
      } else if(event === 'align-right'){
        activeObj.textAlign = 'right';
      } else if(event === 'align-center'){
        activeObj.textAlign = 'center'
      } else if(event === 'align-left'){
        activeObj.textAlign = 'left'
      } else if(event === 'font-underline'){
        setTextUnderLine(activeObj);
        //if(activeObj.textDecoration == 'underline'){
        //  activeObj.textDecoration = 'normal';
        //} else {
        //  activeObj.textDecoration = 'underline'
        //}
      }
      activeObj.canvas.renderAll();
    }

    function setStyle(object, styleName, value) {
      if (object.setSelectionStyles && object.isEditing && object.selectionEnd > object.selectionStart) {
        var style = { };
        style[styleName] = value;
        object.setSelectionStyles(style);
      }
      else {
        object[styleName] = value;
      }
    }

    function getStyle(object, styleName) {
      return (object.getSelectionStyles && object.isEditing && object.selectionEnd > object.selectionStarts)
        ? object.getSelectionStyles()[styleName]
        : object[styleName];
    }

    function setTextBold(obj) {
      var isBold = (getStyle(obj,'fontWeight') ||'').indexOf('bold') > -1;
      setStyle(obj,'fontWeight',isBold == true ? 'normal' :'bold');
    }

    function setTextSize(obj, size){
      setStyle(obj, 'fontSize', size);
    }
    function setTextColor(obj, color){
      setStyle(obj, 'fill', color);
    }

    function setTextFamily(obj, fontFamily){
      setStyle(obj, 'fontFamily', fontFamily);
    }

    function setTextItalic(obj){
       var isItalic = (getStyle(obj,'fontStyle') ||'').indexOf('italic') > -1;
       setStyle(obj, 'fontStyle', isItalic? 'normal': 'italic');
    }

    function setTextUnderLine(obj){
      var isUnderline = (getStyle(obj,'textDecoration') ||'').indexOf('underline') > -1;
      setStyle(obj, 'textDecoration', isUnderline? '': 'underline');
    }
    //------------------------------------------------------ tool items start

    function removeBackground() {
      vm.currentCanvas.backgroundImage = null;
      backCurrentDesignData();
    }

    function clickBook(book) {
      relayService.putKeyValue('_selectedBook_', book);
      vm.selectedBook = book;
      console.log('click on book');
    }

    function changeTitleFont() {
      console.log('in main controller  change title font ');
    }

    function changeTitleColor() {
      console.log('in main controller change title color')
    }

    function changeAuthorFont() {
      console.log('in main controller  change author font ');
    }

    function changeAuthorColor() {
      console.log('in main controller change author color')
    }

    function changeBackgroundColor() {
      console.log('in main controller change book back color');
      console.log(vm.PhotoBook.backgroundColor);
    }

    function readBook(book) {
      console.log('read book');
      console.log(book);
    }

    function coverImageSelected(item, model) {
      if (item)
        vm.PhotoBook.frontCoverImageIndex = item.index;
    }

    function backCoverImageSelected(item, model) {
      if (item)
        vm.PhotoBook.backCoverImageIndex = item.index;
    }

    function selectLeft() {
      if (vm.currentCanvas == vm.left_canvas) return;
      if (vm.currentCanvas) {
        vm.currentCanvas.deactivateAllWithDispatch().renderAll();
      }
      vm.currentCanvas = vm.left_canvas;
      vm.PhotoBook.setPageActive(vm.PhotoBook.leftDesignPage);
      generatePreviewImage();
    }

    function selectRight() {
      if (vm.currentCanvas == vm.right_canvas) return;
      if (vm.currentCanvas) {
        vm.currentCanvas.deactivateAllWithDispatch().renderAll();
      }
      vm.currentCanvas = vm.right_canvas;
      vm.PhotoBook.setPageActive(vm.PhotoBook.rightDesignPage);
      generatePreviewImage();
    }

    function addImage(imageUrl, isBackground) {
      if (!vm.currentCanvas) return;

      if (!isBackground) {
        vm.currentCanvas.addImageObject(imageUrl);
      } else {
        vm.currentCanvas.addBackgroundImage(imageUrl);
      }
    }

    function frontCoverClick() {
      if(vm.currentCanvas)
        vm.currentCanvas.hideToolItems();
      vm.contentPageMode = false;
      vm.dedicatePageMode = false;
      vm.frontCoverMode = true;
      vm.backCoverMode = false;

      backCurrentDesignData();
      vm.PhotoBook.setFrontCoverActive();
    }

    function dedicatedPageClick() {
      vm.currentCanvas.hideToolItems();
      vm.contentPageMode = false;
      vm.dedicatePageMode = true;
      vm.frontCoverMode = false;
      vm.backCoverMode = false;

      backCurrentDesignData();
      vm.PhotoBook.setDedicatedPageActive();
    }

    function backCoverClick() {
      vm.contentPageMode = false;
      vm.dedicatePageMode = false;
      vm.frontCoverMode = false;
      vm.backCoverMode = true;

      backCurrentDesignData();
      vm.PhotoBook.setBackCoverActive();
    }

    function previewClick(page, which) {
      vm.currentCanvas.hideToolItems();
      vm.contentPageMode = true;
      vm.dedicatePageMode = false;
      vm.frontCoverMode = false;
      vm.backCoverMode = false;

      backCurrentDesignData();
      vm.PhotoBook.setPageActive(page);
      if (which === 'left') {
        vm.currentCanvas = vm.left_canvas;
        vm.PhotoBook.leftDesignPage = page;
        vm.PhotoBook.rightDesignPage = vm.PhotoBook.getNextPage(page);
      } else if (which == 'right') {
        vm.currentCanvas = vm.right_canvas;
        vm.PhotoBook.leftDesignPage = vm.PhotoBook.getPreviousPage(page);
        vm.PhotoBook.rightDesignPage = page;
      }
      vm.left_canvas.clear();
      vm.right_canvas.clear();

      vm.restoreToCurrentDesignData();
      //$scope.$apply();
    }

    function nextPage() {
      vm.left_canvas.clear();
      vm.right_canvas.clear();
      vm.PhotoBook.MoveToNextPage();
      vm.restoreToCurrentDesignData();
    }

    function previousPage(currentIndex) {
      vm.PhotoBook.MoveToPreviousPage();
      vm.restoreToCurrentDesignData();
    }

    function drawMode(){
      // TODO
       this.left_canvas.isDrawingMode = ! this.left_canvas.isDrawingMode;
       this.right_canvas.isDrawingMode = ! this.right_canvas.isDrawingMode;
    }

    function backCurrentDesignData() {
      if (!vm.PhotoBook || !vm.PhotoBook || !vm.PhotoBook.leftDesignPage) return;

      if(vm.left_canvas && vm.left_canvas.getObjects().length > 0)
        vm.PhotoBook.leftDesignPage.imageData = JSON.stringify(vm.left_canvas); //vm.left_canvas.toJSON();   //

      if (vm.PhotoBook.rightDesignPage && ( vm.right_canvas && vm.right_canvas.getObjects().length > 0) ) {
        vm.PhotoBook.rightDesignPage.imageData = JSON.stringify(vm.right_canvas);
      }

      if (vm.frontCoverCanvas && vm.PhotoBook.frontCover){
        vm.PhotoBook.frontCover.imageData = JSON.stringify(vm.frontCoverCanvas);
      }

      if (vm.backCoverCanvas && vm.PhotoBook.backCover){
         vm.PhotoBook.backCover.imageData = JSON.stringify(vm.backCoverCanvas);
      }

      if (vm.dedicatePageCanvas && vm.PhotoBook.dedicatedPage){
         vm.PhotoBook.dedicatedPage.imageData = JSON.stringify(vm.dedicatePageCanvas);
      }

      generatePreviewImage();
    }

    function restoreCoversData(){
      var frontCoverData = vm.PhotoBook.frontCover.imageData;
      restoreCanvasData(vm.frontCoverCanvas,frontCoverData, 'frontCoverCanvas');

      // var backCoverData = vm.PhotoBook.backCover.imageData;
      // restoreCanvasData(vm.backCoverCanvas,backCoverData);

      //var dedicatePageData = vm.PhotoBook.dedicatePage.imageData;
      //restoreCanvasData(vm.dedicatePageCanvas,dedicatePageData, 'dedicatePageCanvas');
    }

    function restoreCanvasData(theCanvas, data, canvasName){
      if(theCanvas && data){
        theCanvas.loadFromJSON(data,
          theCanvas.renderAll.bind(theCanvas)
        )
        if(theCanvas != vm.frontCoverCanvas)
        theCanvas.setZoom(scale);
      }
    }

    function restoreToCurrentDesignData() {

      if (vm.PhotoBook.leftDesignPage) {
        var leftData = vm.PhotoBook.leftDesignPage.imageData;
        restoreCanvasData(vm.left_canvas,leftData, 'leftCanvas');
      }

      if (vm.PhotoBook.rightDesignPage) {
        var rightData = vm.PhotoBook.rightDesignPage.imageData;
        restoreCanvasData(vm.right_canvas,rightData, 'rightCanvas');
      }
    }

    function generatePreviewImage() {

        if (vm.left_canvas && vm.PhotoBook.leftDesignPage) {
          vm.PhotoBook.leftDesignPage.previewImage = vm.left_canvas.toDataURL();
        }

        if (vm.PhotoBook.rightDesignPage && vm.right_canvas) {
          vm.PhotoBook.rightDesignPage.previewImage = vm.right_canvas.toDataURL();
        }

        if (vm.frontCoverCanvas && vm.PhotoBook.frontCover)
          vm.PhotoBook.frontCover.previewImage = vm.frontCoverCanvas.toDataURL();

        if (vm.dedicatePageCanvas && vm.PhotoBook.dedicatedPage)
          vm.PhotoBook.frontCover.previewImage = vm.frontCoverCanvas.toDataURL();

        if (vm.backCoverCanvas && vm.PhotoBook.backCover) {
          vm.PhotoBook.backCover.previewImage = vm.backCoverCanvas.toDataURL();
        }
    }

    function deleteObject() {
      //if(vm.PhotoBook.leftDesignPage.active)
      vm.PhotoBook.deletePage(vm.PhotoBook.leftDesignPage);
    }

    function newPage() {
      vm.PhotoBook.createPage();
    }

    function copyPage() {
      backCurrentDesignData();
      vm.PhotoBook.copyPage(vm.currentPage);
      vm.restoreToCurrentDesignData();
    }

    function deletePage() {
      vm.left_canvas.clear();
      vm.right_canvas.clear();
      vm.PhotoBook.deletePage(vm.currentPage);
      vm.restoreToCurrentDesignData();
    }

    function saveToServe() {
      $scope.loginLoading = true;
      backCurrentDesignData();
      generatePreviewImage();
      bookRepository.saveToServer(vm.PhotoBook)
        .then(function(){
          $scope.loginLoading = false;
        });
    }

    function finishCreateBook() {
      $state.go('app.mybooks');
    }

    function nextSaveToImage() {
      $scope.preview_image = vm.left_canvas.toDataURL();
      //var preview_image = $document.find('#preview_image');
      var preview_image = $(event.target).find('#preview_image');
      preview_image = $("#preview_image");
      preview_image.src = image;
      preview_image.attr('src', image);
    }

    $scope.safeApply = function (fn) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    }

    function setFrontCanvas(c){
      vm.frontCoverCanvas = c;
      restoreBoook();
    }
    function getCurrentCanvas(){
      if(vm.contentPageMode=== true){
        if(vm.PhotoBook.leftDesignPage.active){
          vm.currentCanvas = vm.left_canvas;
        } else {
          vm.currentCanvas = vm.right_canvas;
        }

        return vm.currentCanvas;
      } else if(vm.frontCoverMode == true){
        return vm.frontCoverCanvas;
      } else if (vm.dedicatePageMode == true){
        return vm.dedicatePageCanvas;
      }
    }
    $timeout(documentReady,3);

    function documentReady() {
    }
  }
})();
