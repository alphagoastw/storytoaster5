(function() {
  'use strict';

  angular.module('storyToaster', [
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'ngMessages',
    'uiGmapgoogle-maps',
    'angular-cache',
    'ngTable',
    'ngSanitize',
    'ngStorage',
    'ng.deviceDetector',
    'toastr',
    'ngAnimate',
    'angularMoment',
    'prettyXml',
    'xeditable',
    'oitozero.ngSweetAlert',
    'ngCookies',
    'ui.select',
    'minicolors',
    'ngjsColorPicker',
    'colorpicker.module',
    'angular-ladda',
    'angular-lodash'

    ]);//end module()

})();
