(function () {
  'use strict';

  angular.module('storyToaster').config(logConfig);

  function logConfig($provide) {
    $provide.decorator('$log', logDecoratorFactory);
  }

  function logDecoratorFactory($delegate) {
    var methods = ['log', 'info', 'warn', 'error', 'debug'];

    function LogDecorator(tag) {
      this.$delegate = $delegate;
      this.tag = tag;
    }

    methods.forEach(function (method) {
      LogDecorator.prototype[method] = getDelegatedMethod(method);
    });
    LogDecorator.prototype.withTag = withComponent;

    return new LogDecorator('Default');

    // ------- Implementation ------

    function getDelegatedMethod(methodName) {
      var wrappedMethod = $delegate[methodName];
      return function () {
        var args = Array.prototype.slice.call(arguments);
        var self = this;
        if (!wrappedMethod) {
          console.error('Cannot find method <' + methodName + '> in $log');
        } else {
          wrappedMethod.apply($delegate, logFacade(self, methodName, args));
        }
      };
    }

    function logFacade(logger, method, args) {
      var tag = logger.tag;
      tag = '[' + tag + '] ';
      if (angular.isArray(args) && args.length > 0) {
        var first = args[0];
        if (angular.isString(first)) {
          args[0] = tag + first;
        } else {
          args.splice(0, 0, tag);
        }
        return args;
      } else {
        return args;
      }
    }

    function withComponent(tag) {
      if (angular.isFunction(tag)) {
        return new LogDecorator(tag.name);
      } else if (angular.isObject(tag)) {
        return new LogDecorator(tag.constructor.name);
      } else {
        return new LogDecorator(tag);
      }
    }
  }

})();
